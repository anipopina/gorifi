/*global route, swal, myswal, Util, Chart */

const TAGG_TIME = new Date().getTime();
const WIDTH_PC = 1024, WIDTH_TABLET = 720, WIDTH_PHONE = 420;

const Monaparty = require('../mymodules/Monaparty.js');
Monaparty.timeout = 30000;
Monaparty.aParty = Monaparty.aCounterPartyRequest;
Monaparty.aBlock = Monaparty.aCounterBlockRequest;
Monaparty.aPartyTableAll = Monaparty.aCounterPartyRequestTableAll;

const TagG = {
    query: route.query() || {},
    jp: (['ja', 'ja-JP'].includes(navigator.language)),
    asset2info: {},
    asset2card: {},
    asset2rakugakiTitle: {},
    asset2treasureQtys: {},
    asset2isAnime: {},
    quoteAsset: 'XMP',
    isQuoteAssetDivisible: true,
    wallet: null, // プラグインを発見したらnullじゃなくなる
    address: null, // ウォレットアクセスが許可されたらnullじゃなくなる
    asset2qty: {},

    SWAL_TIMER: 2000,
    SATOSHI_DECIMAL: 8,
    SATOSHI_RATIO: 1e8,
    FEESAT_PERBYTE: 200,
    AUCTIONFLAG_PRICE: 1e9,
    BASECHAIN_ASSET: 'MONA',
    IS_PREVIEW: (location.hostname.indexOf('localhost') !== -1),
    IS_LAZYLOADAVAILABLE: ('loading' in HTMLImageElement.prototype),
    RELATIVE_URL_BASE: '/',
    RELATIVE_LINK_BASE: '/',
    QUERYKEY_SEARCH: 'search',
    STORAGEKEY_RECENTBASES: 'gorifi_recentbases',
    TWITTER_USERURL_FORMAT: 'https://twitter.com/{SN}',
    TWITTER_TWEETURL_FORMAT: 'https://twitter.com/{SN}/status/{TWEETID}',
    TWITTER_OPENTWEETURL_FORMAT: 'https://twitter.com/intent/tweet?text={URIENCODEDTXT}',
    ADDR_PARTYEXPURL_FORMAT: 'https://mpchain.info/address/{ADDR}',
    ASSET_PARTYEXPURL_FORMAT: 'https://mpchain.info/asset/{ASSET}',
    MONACHARAT_ASSETGROUPS: ['MonaCharaT'],
    MONACHARAT_IMGROOT: 'https://monacharat-img.komikikaku.com/',
    MONACHARAT_SITEROOT: 'https://monacharat.komikikaku.com/',
    RAKUGAKI_ASSETGROUPS: ['Rakugaki'],
    RAKUGAKI_IMGROOT: 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/rakugaki/',
    RAKUGAKI_JSON: 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/public/rakugaki.json',
    MONACUTE_ASSETGROUPS: ['Monacute'],
    MONACUTE_URLFORMAT: 'https://monacute.art/monacutes/{ID}',
    TREASURES_JSON: 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/public/tokenvault.json',
    TREASURES_URL_FORMAT: 'https://monapalette.komikikaku.com/tokenvault?search={ASSET}',
    ANIMELIST_JSON: 'https://monapachan.s3.ap-northeast-1.amazonaws.com/animelist.json',
    DEXASSETS_CACHEJSON: 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dexassetscache.json',
    CARDSERVER_API: 'https://card.mona.jp/api/',
    CARD_DETAILURL_FORMAT: 'https://card.mona.jp/explorer/card_detail?asset={ASSETNAME}',
    DONATION_ADDR: 'MHASWEi8SPJEWvw1JVv8mGVFnWRRLKiToz',
};
for (const key in TagG.query) TagG.query[key] = TagG.query[key].split('#')[0];
if (TagG.IS_PREVIEW) {
    TagG.RELATIVE_URL_BASE = '';
    TagG.RELATIVE_LINK_BASE = '#';
}

TagG.aMonapartyOrder = async(giveAsset, giveSat, getAsset, getSat) => {
    try {
        TagG.bodySpinner.start();
        const params = {
            'source': TagG.address,
            'give_asset': giveAsset,
            'give_quantity': giveSat,
            'get_asset': getAsset,
            'get_quantity': getSat,
            'expiration': 65535,
            'fee_provided': 0, // ドキュメントにないけど必須
            'fee_required': 0, // ドキュメントにないけど必須
            'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
            'allow_unconfirmed_inputs': true,
        };
        console.log(params);
        const txHex = await Monaparty.aParty('create_order', params);
        const txID = await TagG.wallet.sendRawTransaction(txHex);
        console.log({ txID });
        TagG.asset2qty[giveAsset] -= giveSat;
        myswal({ title: 'Success', text: (TagG.jp ? '注文しました' : 'Ordered.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        return true;
    }
    catch (error) {
        console.error(error);
        myswal('Error', TagG.jp ? `注文できませんでした。ヒント: ${error}` : `Failed to order. hint: ${error}`, 'error');
        return false;
    }
    finally { TagG.bodySpinner.stop() }
};
TagG.aMonapartyCancel = async(targetTxID) => {
    try {
        TagG.bodySpinner.start();
        const params = {
            'source': TagG.address,
            'offer_hash': targetTxID,
            'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
            'allow_unconfirmed_inputs': true,
        };
        console.log(params);
        const txHex = await Monaparty.aParty('create_cancel', params);
        const txID = await TagG.wallet.sendRawTransaction(txHex);
        console.log({ txID });
        myswal({ title: 'Success', text: (TagG.jp ? '注文をキャンセルしました' : 'Order deleted.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        return true;
    }
    catch (error) {
        console.error(error);
        myswal('Error', TagG.jp ? `注文をキャンセルできませんでした。ヒント: ${error}` : `Failed to delete order. hint: ${error}`, 'error');
        return false;
    }
    finally { TagG.bodySpinner.stop() }
};
{
    TagG.aLoadWallet = async(isManual) => {
        try {
            TagG.bodySpinner.start();
            if (!TagG.wallet) await aFindWalletPlugin();
            if (!TagG.wallet) return;
            TagG.address = await TagG.wallet.getAddress();
            TagG.shortenAddr = `${TagG.address.slice(0, 4)}...${TagG.address.slice(-4)}`;
            TagG.addrExpURL = TagG.ADDR_PARTYEXPURL_FORMAT.replace('{ADDR}', TagG.address);
            await TagG.aUpdateBalances();
            TagG.body.update();
            if (isManual && TagG.onLoadWalletManually) TagG.onLoadWalletManually();
        }
        catch (error) {
            console.error(error);
            myswal('Error', error, 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aFindWalletPlugin = async() => {
        if (window == window.parent) {
            if (window.mpurse) TagG.wallet = window.mpurse;
        }
        else { // iframe
            window.monapaletteConnect.getAddress().then(() => TagG.wallet = window.monapaletteConnect).catch(() => {}); // 注意: AppConnectじゃない場合にawaitすると返ってこない
            for (let i = 0; i < 10; i++) {
                if (TagG.wallet) break;
                await Util.aSleep(100);
            }
        }
    };
}
{
    let lastTime = 0;
    TagG.aUpdateBalances = async() => {
        if (!TagG.address) return;
        const nowTime = new Date().getTime();
        if (lastTime > nowTime - 90e3) return;
        lastTime = nowTime;
        const balanceFilters = [{ field: 'address', op: '==', value: TagG.address }];
        const balances = await Monaparty.aPartyTableAll('get_balances', { filters: balanceFilters });
        for (const bal of balances) TagG.asset2qty[bal.asset] = bal.quantity;
        await TagG.aLoadAssets(balances.map(bal => bal.asset));
    };
}
TagG.getAssetImageURL = (size, asset) => { // size = 'L'/'M'/'S'/'auto', assetはnameでも可
    const assetInfo = TagG.asset2info[asset];
    if (!assetInfo) return null;
    if (assetInfo.appData.monacharat) {
        switch (size) {
            case 'L': case 'M': case 'auto': return assetInfo.appData.monacharat.image;
            case 'S': return assetInfo.appData.thumb;
        }
    }
    if (assetInfo.appData.rakugakinft) return assetInfo.appData.rakugakinft.image;
    const cardData = TagG.asset2card[assetInfo.name];
    if (cardData) {
        switch (size) {
            case 'L': return cardData.imgurURL;
            case 'M': return cardData.imgurSizedURL;
            case 'S': return cardData.imgurSmallURL;
            case 'auto':
                if (TagG.asset2isAnime[cardData.asset]) return cardData.imgurURL;
                else return cardData.imgurSizedURL;
        }
    }
    return null;
};
{
    TagG.aLoadAssetsInfo = async(assets) => { // assetNameでもOK
        const filteredAssets = assets.filter(asset => !TagG.asset2info[asset]);
        const assetsInfo = await aGetManyAssetsInfo(filteredAssets);
        TagG.addAssetsInfo(assetsInfo);
    };
    TagG.addAssetsInfo = async(assetsInfo) => {
        for (const info of assetsInfo) {
            const name = info.asset_longname || info.asset;
            const group = info.assetgroup || info.asset_group || null; // 表記ゆれ対策
            let parsedDesc = null;
            if (info.description.trim()[0] === '{') { // 配列や""で囲われた文字列もparseできてしまうので{}だけに限定
                try { parsedDesc = JSON.parse(info.description) } catch (err) {}
            }
            const appData = {};
            if (parsedDesc?.attr) {
                const attributes = [];
                for (const name in parsedDesc.attr) {
                    const value = parsedDesc.attr[name];
                    if (['string', 'number'].includes(typeof value)) attributes.push({ name, value })
                }
                if (attributes.length) appData.attributes = attributes;
            }
            if (group) {
                if (TagG.MONACHARAT_ASSETGROUPS.includes(group)) appData.monacharat = { // Monacharat
                    url: `${TagG.MONACHARAT_SITEROOT}${info.asset}`,
                    image: `${TagG.MONACHARAT_IMGROOT}${name}.png`,
                    thumb: `${TagG.MONACHARAT_IMGROOT}${name}_160.png`,
                };
                if (TagG.RAKUGAKI_ASSETGROUPS.includes(group)) appData.rakugakinft = { // RakugakiNFT
                    image: `${TagG.RAKUGAKI_IMGROOT}${name}`,
                };
                if (TagG.MONACUTE_ASSETGROUPS.includes(group) && parsedDesc?.monacute) appData.monacute = { // Monacute
                    url: TagG.MONACUTE_URLFORMAT.replace('{ID}', parsedDesc.monacute.id),
                };
            }
            Object.assign(info, { // format: assetInfo
                name,
                shortenName: TagG.shortenAsset(name),
                group,
                issuedSat: info.supply,
                issuedAmount: TagG.qty2amount(info.supply, info.divisible),
                isDivisible: info.divisible,
                isReassignable: info.reassignable,
                isListed: info.listed,
                isLocked: info.locked,
                parsedDesc,
                explorerURL: TagG.ASSET_PARTYEXPURL_FORMAT.replace('{ASSET}', info.asset),
                isSquare: Boolean(appData.monacharat || appData.rakugakinft),
                appData,
            });
            TagG.asset2info[info.asset] = TagG.asset2info[info.name] = info;
        }
    };
    const aGetManyAssetsInfo = async(assets) => {
        const ONCE = 250;
        const promises = [];
        for (let i = 0; i < Math.ceil(assets.length / ONCE); i++) {
            const slicedAssets = assets.slice(ONCE * i, ONCE * (i + 1));
            promises.push(Monaparty.aBlock('get_assets_info', { assetsList: slicedAssets }));
        }
        const assetsInfoArray = await Promise.all(promises);
        const assetsInfo = assetsInfoArray.reduce((joined, nextList) => joined.concat(nextList), []);
        return assetsInfo;
    };
}
{
    const IMG_URL_FORMAT_ORIGINAL = 'https://mcspare.nachatdayo.com/image_server/img/{CID}';
    const IMG_URL_FORMAT_SIZED = 'https://monacard-img.komikikaku.com/{CID}l';
    const IMG_URL_FORMAT_SMALL = 'https://monacard-img.komikikaku.com/{CID}t';
    TagG.cardLoadedMap = {};
    TagG.aLoadCardsData = async(assetNames) => {
        const URL = `${TagG.CARDSERVER_API}card_detail_post`;
        const filteredAssetNames = assetNames.filter(name => !TagG.cardLoadedMap[name]);
        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' },
            body: `assets=${encodeURIComponent(filteredAssetNames.join(','))}`,
        };
        let { details } = await fetch(URL, fetchParams).then(res => res.json());
        TagG.addCardsData(details || []);
        for (const name of filteredAssetNames) TagG.cardLoadedMap[name] = true; // 冗長だけど両方必要 *
    };
    TagG.addCardsData = (cardDetails) => {
        for (const detail of cardDetails) {
            TagG.asset2card[detail.asset] = TagG.asset2card[detail.asset_common_name] = { // format: cardData
                asset: detail.asset,
                cardName: unescapeHTML(detail.card_name),
                ownerName: unescapeHTML(detail.owner_name),
                ownerSN: detail.tw_name,
                siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', detail.asset_common_name),
                imgurURL: IMG_URL_FORMAT_ORIGINAL.replace('{CID}', detail.cid), // Thanks to Imgur
                imgurSizedURL: IMG_URL_FORMAT_SIZED.replace('{CID}', detail.cid),
                imgurSmallURL: IMG_URL_FORMAT_SMALL.replace('{CID}', detail.cid),
                description: unescapeHTML(detail.add_description),
                version: detail.ver,
                tags: unescapeHTML(detail.tag),
            };
            TagG.cardLoadedMap[detail.asset_common_name] = true; // 冗長だけど両方必要 *
        }
    };
    const unescapeHTML = (str) => {
        return str.replace(/&(amp|quot|#039|lt|gt);/g, match => {
            return { '&amp;': '&', '&quot;': '"', '&#039;': "'", '&lt;': '<', '&gt;': '>' }[match];
        });
    };
}
TagG.aLoadAssets = async(assets) => { // longnameでもOK
    assets = Util.uniq(assets);
    await TagG.aLoadAssetsInfo(assets);
    const assetNames = assets.map(asset => (TagG.asset2info[asset] || {}).name).filter(val => val);
    await TagG.aLoadCardsData(assetNames).catch(console.error);
};
TagG.aLoadRakugakiTitles = async() => {
    const { asset2title } = await Util.aGetJSON(`${TagG.RAKUGAKI_JSON}?${TAGG_TIME}`, true);
    TagG.asset2rakugakiTitle = asset2title;
};
TagG.aLoadTreasuresList = async() => {
    const { treasures } = await Util.aGetJSON(TagG.TREASURES_JSON, true);
    TagG.asset2treasureQtys = {};
    for (const tre of treasures) {
        if (!TagG.asset2treasureQtys[tre.asset]) TagG.asset2treasureQtys[tre.asset] = [];
        TagG.asset2treasureQtys[tre.asset].push(tre.qty);
    }
};
TagG.aLoadAnimeList = async() => {
    const { animeAssets } = await Util.aGetJSON(`${TagG.ANIMELIST_JSON}?${TAGG_TIME}`, true);
    TagG.asset2isAnime = {};
    for (const asset of animeAssets) TagG.asset2isAnime[asset] = true;
};
TagG.aLoadCachedAssetData = async() => {
    const promises = [];
    // アセット情報は上書きされるので完全更新の頻度が高いやつを後ろに
    if (TagG.DEXASSETS_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DEXASSETS_CACHEJSON}?${TagG.getCacheQuery(20)}`).catch(() => ({})));
    const cacheJsons = await Promise.all(promises);
    for (const { assetsInfo, monacards, noCards } of cacheJsons) {
        if (!assetsInfo) continue;
        TagG.addAssetsInfo(assetsInfo.filter(info => !TagG.asset2info[info.asset]));
        TagG.addCardsData(monacards.filter(card => !TagG.asset2card[card.asset_common_name]));
        for (const name of noCards) TagG.cardLoadedMap[name] = true;
    }
};
TagG.qty2amount = (qty, isDivisible) => (isDivisible ? qty / TagG.SATOSHI_RATIO : qty);
TagG.amount2qty = (amount, isDivisible) => Math.round(isDivisible ? amount * TagG.SATOSHI_RATIO : amount);
TagG.shortenAsset = (assetName) => (assetName[0] === 'A' ? `${assetName.slice(0, 5)}...${assetName.slice(-4)}` : assetName);
TagG.getCacheQuery = (minutes) => Math.floor(new Date().getTime() / (60e3 * minutes));

if (!TagG.IS_PREVIEW) route.base('/');
window.myswal = async(...args) => { // オリジナルパラメータ: isCutin
    if (!myswal.paramsStack) myswal.paramsStack = [];
    let params;
    if (args.length === 1) {
        if (typeof args[0] === 'string') params = { text: args[0], className: 'swalcustom-singlebutton' };
        else {
            params = args[0];
            if (!('className' in params)) {
                if ('content' in params) params.className = 'swalcustom-content';
                else if ('icon' in params) params.className = 'swalcustom-icon';
                else if (!('buttons' in params)) params.className = 'swalcustom-singlebutton';
                else if (!('title' in params) && ('buttons' in params) && Object.keys(params.buttons).length === 0)
                    params.className = 'swalcustom-justtext';
            }
        }
    }
    else if (args.length === 2) params = { title: args[0], text: args[1], className: 'swalcustom-singlebutton' };
    else if (args.length === 3) params = { title: args[0], text: args[1], icon: args[2], className: 'swalcustom-icon' };
    else params = args; // このときだけarray
    myswal.paramsStack.push(params);
    let swalReturn;
    if (Array.isArray(params)) swalReturn = await swal(...params);
    else swalReturn = await swal(params);
    // swalの仕様によりswal中にswalされた場合はpromiseがpendingのままになるので、その場合はこの先は実行されない
    myswal.paramsStack.pop();
    const lastParams = myswal.paramsStack.pop();
    if (params.isCutin && lastParams) {
        if (Array.isArray(lastParams)) myswal(...lastParams); // awaitしない
        else myswal(lastParams);
    }
    else myswal.paramsStack = [];
    return swalReturn;
};
TagG.asset2card['MONANA'] = {
    cardName: 'MONANA',
    ownerName: 'unknown',
    ownerSN: '',
    siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', 'MONANA'),
    imgurURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA.png`,
    imgurSizedURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_preview.png`,
    imgurSmallURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_small.png`,
    description: 'モナカードではありませんがDEXでよく利用されるトークンなので専用の画像を表示しています',
    version: '1',
    tags: '',
};

TagG.aLoadRakugakiTitles().catch(console.warn);
TagG.aLoadTreasuresList().catch(console.warn);
TagG.aLoadAnimeList().catch(console.warn);
TagG.aLoadCachedAssetData().catch(console.warn);
if (!TagG.IS_LAZYLOADAVAILABLE) myswal({
    title: (TagG.jp ? 'ご注意！' : 'CAUTION!'),
    text: (TagG.jp ? 'このブラウザではGoriFiの一部のリスト表示で件数が制限されるため、他のブラウザを使ったほうがよいです。モダンなブラウザであればだいたい大丈夫ですが、推奨はChromeです。' : 'As this browser can\'t display all items of some list in GoriFi, it is better to use another browser. A modern browser is usually fine, but Chrome is recommended.'),
    icon: 'warning',
    isCutin: true,
});
