/* global riot, swal, TweenLite, mdc, BigInt, QRCode, Spinner, myswal */
// version: 20211103
const Util = {};
Util.aSleep = (msec) => new Promise(resolve => setTimeout(resolve, msec));
Util.uniq = (array) => Array.from(new Set(array));
Util.round = (num, decimal) => Math.round(num * Math.pow(10, decimal)) / Math.pow(10, decimal);
Util.randomInt = (min, max) => Math.floor(Math.random() * (max + 1 - min)) + min;
Util.reverseStr = (str) => str.split('').reverse().join('');
Util.zeroPadding = (num, length) => ('0'.repeat(length) + num).slice(-length);
Util.saveStorage = Util.saveObjectToLocalStorage = (key, object) => {
    localStorage.setItem(key, JSON.stringify(object));
};
Util.loadStorage = Util.loadObjectFromLocalStorage = (key) => {
    const value = localStorage.getItem(key);
    if (!value) return null;
    try {
        if (value[0] === '%') return JSON.parse(decodeURIComponent(value)); // 旧版との互換用
        else return JSON.parse(value)
    }
    catch (error) { console.error(error); return null }
};
Util.floorLargeInt = (largeInt) => {
    return (largeInt > Number.MAX_SAFE_INTEGER)?
        Math.floor(largeInt * ((Number.MAX_SAFE_INTEGER - 2) / Number.MAX_SAFE_INTEGER)) : largeInt;
};
Util.ref2array = (ref) => {
    if (Array.isArray(ref)) return ref;
    if (!ref) return [];
    return [ref];
};
Util.dictizeListWithKey = (list, key) => {
    const dict = {};
    for (const item of list) item[key] && (dict[item[key]] = item);
    return dict;
};
Util.shuffle = (array) => {
    let n = array.length;
    while (n) {
        const i = Math.floor(Math.random() * n--);
        [array[n], array[i]] = [array[i], array[n]];
    }
};
Util.execCopy = string => {
    const tmp = document.createElement('div');
    const style = tmp.style;
    let success = false;
    tmp.appendChild(document.createElement('pre')).textContent = string;
    style.position = 'fixed';
    style.left = '-100%';
    document.body.appendChild(tmp);
    document.getSelection().selectAllChildren(tmp);
    success = document.execCommand('copy'); // 成功でtrue、失敗or未対応だとfalse
    document.body.removeChild(tmp);
    return success;
};
Util.commafy = (num, { decimal = 8, removeDecimalZero = true, noComma = false } = {}) => {
    num = Number(num);
    if (isNaN(num)) return null;
    const parts = num.toFixed(decimal).split('.');
    if (!noComma) parts[0] = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    if (parts[1] && removeDecimalZero) parts[1] = parts[1].replace(/0+$/, '');
    return parts[1] ? parts.join('.') : parts[0];
};
Util.escapeHTML = (str) => {
    return str.replace(/[&'`"<>]/g, match => {
        return { '&': '&amp;', "'": '&#x27;', '`': '&#x60;', '"': '&quot;', '<': '&lt;', '>': '&gt;' }[match];
    });
};
Util.aTimeout = (ms, promise) => {
    return new Promise((resolve, reject) => {
        const timeoutID = setTimeout(() => reject(new Error('timeout by aTimeout()')), ms);
        promise.then(resolve).catch(reject).finally(() => { clearTimeout(timeoutID) });
    });
};
Util.aFetchWithTimeout = async(timeoutMs, url, fetchParams) => {
    return await Util.aTimeout(timeoutMs, fetch(url, fetchParams));
};
Util.aRedundantFetch = async(timeoutMs, urls, fetchParams) => {
    let error;
    for (const url of urls) {
        try {
            const response = await Util.aFetchWithTimeout(timeoutMs, url, fetchParams);
            if (!response.ok) throw new Error(`aRedundantFetch response was not ok: ${url}`);
            return response;
        }
        catch (_error) { console.error(error = _error) }
    }
    throw error;
};
Util.aGetJSON = async(url, useCache = false) => {
    if (!Util.aGetJSON.cacheDict) Util.aGetJSON.cacheDict = {};
    if (useCache && (url in Util.aGetJSON.cacheDict)) return Util.aGetJSON.cacheDict[url];
    try {
        console.log('aGetJSON: ' + url);
        const response = await fetch(url);
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const json = await response.json();
        if (useCache) Util.aGetJSON.cacheDict[url] = json;
        else delete Util.aGetJSON.cacheDict[url];
        return json;
    }
    catch (error) {
        delete Util.aGetJSON.cacheDict[url];
        throw error;
    }
};
Util.aGetNumber = async(url, useCache = false) => {
    if (!Util.aGetNumber.cacheDict) Util.aGetNumber.cacheDict = {};
    if (useCache && (url in Util.aGetNumber.cacheDict)) return Util.aGetNumber.cacheDict[url];
    try {
        console.log('aGetNumber: ' + url);
        const response = await fetch(url);
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const number = Number(await response.text());
        if (useCache) Util.aGetNumber.cacheDict[url] = number;
        else delete Util.aGetNumber.cacheDict[url];
        return number;
    }
    catch (error) {
        delete Util.aGetNumber.cacheDict[url];
        throw error;
    }
};
Util.aJsonRPCRequest = async(url, method, params = {}, useCache = false) => {
    if (!Util.aJsonRPCRequest.cacheDict) Util.aJsonRPCRequest.cacheDict = {};
    const headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' };
    const body = JSON.stringify({ id: 1, method, params });
    const cacheKey = `${url},${method},${body}`;
    if (useCache && Util.aJsonRPCRequest.cacheDict[cacheKey]) return Util.aJsonRPCRequest.cacheDict[cacheKey];
    try {
        console.log(`aJsonRPCRequest: ${url} ${method}`);
        const response = await fetch(url, { method: 'POST', headers, body });
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const parsedResponse = await response.json();
        if (parsedResponse.error) throw parsedResponse.error;
        if (useCache) Util.aJsonRPCRequest.cacheDict[cacheKey] = parsedResponse.result;
        else delete Util.aJsonRPCRequest.cacheDict[cacheKey];
        return parsedResponse.result;
    }
    catch (error) {
        delete Util.aJsonRPCRequest.cacheDict[cacheKey];
        throw error;
    }
};
Util.aOpenContentSwal = (contentDiv, additionalOptions = {}) => {
    if (!contentDiv) return;
    const options = { content: contentDiv, button: false, ...additionalOptions };
    if (window.myswal) return window.myswal(options);
    else return window.swal(options);
};
/*global route, swal, myswal, Util, Chart */

const TAGG_TIME = new Date().getTime();
const WIDTH_PC = 1024, WIDTH_TABLET = 720, WIDTH_PHONE = 420;

const Monaparty = require('../mymodules/Monaparty.js');
Monaparty.timeout = 30000;
Monaparty.aParty = Monaparty.aCounterPartyRequest;
Monaparty.aBlock = Monaparty.aCounterBlockRequest;
Monaparty.aPartyTableAll = Monaparty.aCounterPartyRequestTableAll;

const TagG = {
    query: route.query() || {},
    jp: (['ja', 'ja-JP'].includes(navigator.language)),
    asset2info: {},
    asset2card: {},
    asset2rakugakiTitle: {},
    asset2treasureQtys: {},
    asset2isAnime: {},
    quoteAsset: 'XMP',
    isQuoteAssetDivisible: true,
    wallet: null, // プラグインを発見したらnullじゃなくなる
    address: null, // ウォレットアクセスが許可されたらnullじゃなくなる
    asset2qty: {},

    SWAL_TIMER: 2000,
    SATOSHI_DECIMAL: 8,
    SATOSHI_RATIO: 1e8,
    FEESAT_PERBYTE: 200,
    AUCTIONFLAG_PRICE: 1e9,
    BASECHAIN_ASSET: 'MONA',
    IS_PREVIEW: (location.hostname.indexOf('localhost') !== -1),
    IS_LAZYLOADAVAILABLE: ('loading' in HTMLImageElement.prototype),
    RELATIVE_URL_BASE: '/',
    RELATIVE_LINK_BASE: '/',
    QUERYKEY_SEARCH: 'search',
    STORAGEKEY_RECENTBASES: 'gorifi_recentbases',
    TWITTER_USERURL_FORMAT: 'https://twitter.com/{SN}',
    TWITTER_TWEETURL_FORMAT: 'https://twitter.com/{SN}/status/{TWEETID}',
    TWITTER_OPENTWEETURL_FORMAT: 'https://twitter.com/intent/tweet?text={URIENCODEDTXT}',
    ADDR_PARTYEXPURL_FORMAT: 'https://mpchain.info/address/{ADDR}',
    ASSET_PARTYEXPURL_FORMAT: 'https://mpchain.info/asset/{ASSET}',
    MONACHARAT_ASSETGROUPS: ['MonaCharaT'],
    MONACHARAT_IMGROOT: 'https://monacharat-img.komikikaku.com/',
    MONACHARAT_SITEROOT: 'https://monacharat.komikikaku.com/',
    RAKUGAKI_ASSETGROUPS: ['Rakugaki'],
    RAKUGAKI_IMGROOT: 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/rakugaki/',
    RAKUGAKI_JSON: 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/public/rakugaki.json',
    MONACUTE_ASSETGROUPS: ['Monacute'],
    MONACUTE_URLFORMAT: 'https://monacute.art/monacutes/{ID}',
    TREASURES_JSON: 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/public/tokenvault.json',
    TREASURES_URL_FORMAT: 'https://monapalette.komikikaku.com/tokenvault?search={ASSET}',
    ANIMELIST_JSON: 'https://monapachan.s3.ap-northeast-1.amazonaws.com/animelist.json',
    DEXASSETS_CACHEJSON: 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dexassetscache.json',
    CARDSERVER_API: 'https://card.mona.jp/api/',
    CARD_DETAILURL_FORMAT: 'https://card.mona.jp/explorer/card_detail?asset={ASSETNAME}',
    DONATION_ADDR: 'MHASWEi8SPJEWvw1JVv8mGVFnWRRLKiToz',
};
for (const key in TagG.query) TagG.query[key] = TagG.query[key].split('#')[0];
if (TagG.IS_PREVIEW) {
    TagG.RELATIVE_URL_BASE = '';
    TagG.RELATIVE_LINK_BASE = '#';
}

TagG.aMonapartyOrder = async(giveAsset, giveSat, getAsset, getSat) => {
    try {
        TagG.bodySpinner.start();
        const params = {
            'source': TagG.address,
            'give_asset': giveAsset,
            'give_quantity': giveSat,
            'get_asset': getAsset,
            'get_quantity': getSat,
            'expiration': 65535,
            'fee_provided': 0, // ドキュメントにないけど必須
            'fee_required': 0, // ドキュメントにないけど必須
            'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
            'allow_unconfirmed_inputs': true,
        };
        console.log(params);
        const txHex = await Monaparty.aParty('create_order', params);
        const txID = await TagG.wallet.sendRawTransaction(txHex);
        console.log({ txID });
        TagG.asset2qty[giveAsset] -= giveSat;
        myswal({ title: 'Success', text: (TagG.jp ? '注文しました' : 'Ordered.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        return true;
    }
    catch (error) {
        console.error(error);
        myswal('Error', TagG.jp ? `注文できませんでした。ヒント: ${error}` : `Failed to order. hint: ${error}`, 'error');
        return false;
    }
    finally { TagG.bodySpinner.stop() }
};
TagG.aMonapartyCancel = async(targetTxID) => {
    try {
        TagG.bodySpinner.start();
        const params = {
            'source': TagG.address,
            'offer_hash': targetTxID,
            'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
            'allow_unconfirmed_inputs': true,
        };
        console.log(params);
        const txHex = await Monaparty.aParty('create_cancel', params);
        const txID = await TagG.wallet.sendRawTransaction(txHex);
        console.log({ txID });
        myswal({ title: 'Success', text: (TagG.jp ? '注文をキャンセルしました' : 'Order deleted.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        return true;
    }
    catch (error) {
        console.error(error);
        myswal('Error', TagG.jp ? `注文をキャンセルできませんでした。ヒント: ${error}` : `Failed to delete order. hint: ${error}`, 'error');
        return false;
    }
    finally { TagG.bodySpinner.stop() }
};
{
    TagG.aLoadWallet = async(isManual) => {
        try {
            TagG.bodySpinner.start();
            if (!TagG.wallet) await aFindWalletPlugin();
            if (!TagG.wallet) return;
            TagG.address = await TagG.wallet.getAddress();
            TagG.shortenAddr = `${TagG.address.slice(0, 4)}...${TagG.address.slice(-4)}`;
            TagG.addrExpURL = TagG.ADDR_PARTYEXPURL_FORMAT.replace('{ADDR}', TagG.address);
            await TagG.aUpdateBalances();
            TagG.body.update();
            if (isManual && TagG.onLoadWalletManually) TagG.onLoadWalletManually();
        }
        catch (error) {
            console.error(error);
            myswal('Error', error, 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aFindWalletPlugin = async() => {
        if (window == window.parent) {
            if (window.mpurse) TagG.wallet = window.mpurse;
        }
        else { // iframe
            window.monapaletteConnect.getAddress().then(() => TagG.wallet = window.monapaletteConnect).catch(() => {}); // 注意: AppConnectじゃない場合にawaitすると返ってこない
            for (let i = 0; i < 10; i++) {
                if (TagG.wallet) break;
                await Util.aSleep(100);
            }
        }
    };
}
{
    let lastTime = 0;
    TagG.aUpdateBalances = async() => {
        if (!TagG.address) return;
        const nowTime = new Date().getTime();
        if (lastTime > nowTime - 90e3) return;
        lastTime = nowTime;
        const balanceFilters = [{ field: 'address', op: '==', value: TagG.address }];
        const balances = await Monaparty.aPartyTableAll('get_balances', { filters: balanceFilters });
        for (const bal of balances) TagG.asset2qty[bal.asset] = bal.quantity;
        await TagG.aLoadAssets(balances.map(bal => bal.asset));
    };
}
TagG.getAssetImageURL = (size, asset) => { // size = 'L'/'M'/'S'/'auto', assetはnameでも可
    const assetInfo = TagG.asset2info[asset];
    if (!assetInfo) return null;
    if (assetInfo.appData.monacharat) {
        switch (size) {
            case 'L': case 'M': case 'auto': return assetInfo.appData.monacharat.image;
            case 'S': return assetInfo.appData.thumb;
        }
    }
    if (assetInfo.appData.rakugakinft) return assetInfo.appData.rakugakinft.image;
    const cardData = TagG.asset2card[assetInfo.name];
    if (cardData) {
        switch (size) {
            case 'L': return cardData.imgurURL;
            case 'M': return cardData.imgurSizedURL;
            case 'S': return cardData.imgurSmallURL;
            case 'auto':
                if (TagG.asset2isAnime[cardData.asset]) return cardData.imgurURL;
                else return cardData.imgurSizedURL;
        }
    }
    return null;
};
{
    TagG.aLoadAssetsInfo = async(assets) => { // assetNameでもOK
        const filteredAssets = assets.filter(asset => !TagG.asset2info[asset]);
        const assetsInfo = await aGetManyAssetsInfo(filteredAssets);
        TagG.addAssetsInfo(assetsInfo);
    };
    TagG.addAssetsInfo = async(assetsInfo) => {
        for (const info of assetsInfo) {
            const name = info.asset_longname || info.asset;
            const group = info.assetgroup || info.asset_group || null; // 表記ゆれ対策
            let parsedDesc = null;
            if (info.description.trim()[0] === '{') { // 配列や""で囲われた文字列もparseできてしまうので{}だけに限定
                try { parsedDesc = JSON.parse(info.description) } catch (err) {}
            }
            const appData = {};
            if (parsedDesc?.attr) {
                const attributes = [];
                for (const name in parsedDesc.attr) {
                    const value = parsedDesc.attr[name];
                    if (['string', 'number'].includes(typeof value)) attributes.push({ name, value })
                }
                if (attributes.length) appData.attributes = attributes;
            }
            if (group) {
                if (TagG.MONACHARAT_ASSETGROUPS.includes(group)) appData.monacharat = { // Monacharat
                    url: `${TagG.MONACHARAT_SITEROOT}${info.asset}`,
                    image: `${TagG.MONACHARAT_IMGROOT}${name}.png`,
                    thumb: `${TagG.MONACHARAT_IMGROOT}${name}_160.png`,
                };
                if (TagG.RAKUGAKI_ASSETGROUPS.includes(group)) appData.rakugakinft = { // RakugakiNFT
                    image: `${TagG.RAKUGAKI_IMGROOT}${name}`,
                };
                if (TagG.MONACUTE_ASSETGROUPS.includes(group) && parsedDesc?.monacute) appData.monacute = { // Monacute
                    url: TagG.MONACUTE_URLFORMAT.replace('{ID}', parsedDesc.monacute.id),
                };
            }
            Object.assign(info, { // format: assetInfo
                name,
                shortenName: TagG.shortenAsset(name),
                group,
                issuedSat: info.supply,
                issuedAmount: TagG.qty2amount(info.supply, info.divisible),
                isDivisible: info.divisible,
                isReassignable: info.reassignable,
                isListed: info.listed,
                isLocked: info.locked,
                parsedDesc,
                explorerURL: TagG.ASSET_PARTYEXPURL_FORMAT.replace('{ASSET}', info.asset),
                isSquare: Boolean(appData.monacharat || appData.rakugakinft),
                appData,
            });
            TagG.asset2info[info.asset] = TagG.asset2info[info.name] = info;
        }
    };
    const aGetManyAssetsInfo = async(assets) => {
        const ONCE = 250;
        const promises = [];
        for (let i = 0; i < Math.ceil(assets.length / ONCE); i++) {
            const slicedAssets = assets.slice(ONCE * i, ONCE * (i + 1));
            promises.push(Monaparty.aBlock('get_assets_info', { assetsList: slicedAssets }));
        }
        const assetsInfoArray = await Promise.all(promises);
        const assetsInfo = assetsInfoArray.reduce((joined, nextList) => joined.concat(nextList), []);
        return assetsInfo;
    };
}
{
    const IMG_URL_FORMAT_ORIGINAL = 'https://mcspare.nachatdayo.com/image_server/img/{CID}';
    const IMG_URL_FORMAT_SIZED = 'https://monacard-img.komikikaku.com/{CID}l';
    const IMG_URL_FORMAT_SMALL = 'https://monacard-img.komikikaku.com/{CID}t';
    TagG.cardLoadedMap = {};
    TagG.aLoadCardsData = async(assetNames) => {
        const URL = `${TagG.CARDSERVER_API}card_detail_post`;
        const filteredAssetNames = assetNames.filter(name => !TagG.cardLoadedMap[name]);
        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' },
            body: `assets=${encodeURIComponent(filteredAssetNames.join(','))}`,
        };
        let { details } = await fetch(URL, fetchParams).then(res => res.json());
        TagG.addCardsData(details || []);
        for (const name of filteredAssetNames) TagG.cardLoadedMap[name] = true; // 冗長だけど両方必要 *
    };
    TagG.addCardsData = (cardDetails) => {
        for (const detail of cardDetails) {
            TagG.asset2card[detail.asset] = TagG.asset2card[detail.asset_common_name] = { // format: cardData
                asset: detail.asset,
                cardName: unescapeHTML(detail.card_name),
                ownerName: unescapeHTML(detail.owner_name),
                ownerSN: detail.tw_name,
                siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', detail.asset_common_name),
                imgurURL: IMG_URL_FORMAT_ORIGINAL.replace('{CID}', detail.cid), // Thanks to Imgur
                imgurSizedURL: IMG_URL_FORMAT_SIZED.replace('{CID}', detail.cid),
                imgurSmallURL: IMG_URL_FORMAT_SMALL.replace('{CID}', detail.cid),
                description: unescapeHTML(detail.add_description),
                version: detail.ver,
                tags: unescapeHTML(detail.tag),
            };
            TagG.cardLoadedMap[detail.asset_common_name] = true; // 冗長だけど両方必要 *
        }
    };
    const unescapeHTML = (str) => {
        return str.replace(/&(amp|quot|#039|lt|gt);/g, match => {
            return { '&amp;': '&', '&quot;': '"', '&#039;': "'", '&lt;': '<', '&gt;': '>' }[match];
        });
    };
}
TagG.aLoadAssets = async(assets) => { // longnameでもOK
    assets = Util.uniq(assets);
    await TagG.aLoadAssetsInfo(assets);
    const assetNames = assets.map(asset => (TagG.asset2info[asset] || {}).name).filter(val => val);
    await TagG.aLoadCardsData(assetNames).catch(console.error);
};
TagG.aLoadRakugakiTitles = async() => {
    const { asset2title } = await Util.aGetJSON(`${TagG.RAKUGAKI_JSON}?${TAGG_TIME}`, true);
    TagG.asset2rakugakiTitle = asset2title;
};
TagG.aLoadTreasuresList = async() => {
    const { treasures } = await Util.aGetJSON(TagG.TREASURES_JSON, true);
    TagG.asset2treasureQtys = {};
    for (const tre of treasures) {
        if (!TagG.asset2treasureQtys[tre.asset]) TagG.asset2treasureQtys[tre.asset] = [];
        TagG.asset2treasureQtys[tre.asset].push(tre.qty);
    }
};
TagG.aLoadAnimeList = async() => {
    const { animeAssets } = await Util.aGetJSON(`${TagG.ANIMELIST_JSON}?${TAGG_TIME}`, true);
    TagG.asset2isAnime = {};
    for (const asset of animeAssets) TagG.asset2isAnime[asset] = true;
};
TagG.aLoadCachedAssetData = async() => {
    const promises = [];
    // アセット情報は上書きされるので完全更新の頻度が高いやつを後ろに
    if (TagG.DEXASSETS_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DEXASSETS_CACHEJSON}?${TagG.getCacheQuery(20)}`).catch(() => ({})));
    const cacheJsons = await Promise.all(promises);
    for (const { assetsInfo, monacards, noCards } of cacheJsons) {
        if (!assetsInfo) continue;
        TagG.addAssetsInfo(assetsInfo.filter(info => !TagG.asset2info[info.asset]));
        TagG.addCardsData(monacards.filter(card => !TagG.asset2card[card.asset_common_name]));
        for (const name of noCards) TagG.cardLoadedMap[name] = true;
    }
};
TagG.qty2amount = (qty, isDivisible) => (isDivisible ? qty / TagG.SATOSHI_RATIO : qty);
TagG.amount2qty = (amount, isDivisible) => Math.round(isDivisible ? amount * TagG.SATOSHI_RATIO : amount);
TagG.shortenAsset = (assetName) => (assetName[0] === 'A' ? `${assetName.slice(0, 5)}...${assetName.slice(-4)}` : assetName);
TagG.getCacheQuery = (minutes) => Math.floor(new Date().getTime() / (60e3 * minutes));

if (!TagG.IS_PREVIEW) route.base('/');
window.myswal = async(...args) => { // オリジナルパラメータ: isCutin
    if (!myswal.paramsStack) myswal.paramsStack = [];
    let params;
    if (args.length === 1) {
        if (typeof args[0] === 'string') params = { text: args[0], className: 'swalcustom-singlebutton' };
        else {
            params = args[0];
            if (!('className' in params)) {
                if ('content' in params) params.className = 'swalcustom-content';
                else if ('icon' in params) params.className = 'swalcustom-icon';
                else if (!('buttons' in params)) params.className = 'swalcustom-singlebutton';
                else if (!('title' in params) && ('buttons' in params) && Object.keys(params.buttons).length === 0)
                    params.className = 'swalcustom-justtext';
            }
        }
    }
    else if (args.length === 2) params = { title: args[0], text: args[1], className: 'swalcustom-singlebutton' };
    else if (args.length === 3) params = { title: args[0], text: args[1], icon: args[2], className: 'swalcustom-icon' };
    else params = args; // このときだけarray
    myswal.paramsStack.push(params);
    let swalReturn;
    if (Array.isArray(params)) swalReturn = await swal(...params);
    else swalReturn = await swal(params);
    // swalの仕様によりswal中にswalされた場合はpromiseがpendingのままになるので、その場合はこの先は実行されない
    myswal.paramsStack.pop();
    const lastParams = myswal.paramsStack.pop();
    if (params.isCutin && lastParams) {
        if (Array.isArray(lastParams)) myswal(...lastParams); // awaitしない
        else myswal(lastParams);
    }
    else myswal.paramsStack = [];
    return swalReturn;
};
TagG.asset2card['MONANA'] = {
    cardName: 'MONANA',
    ownerName: 'unknown',
    ownerSN: '',
    siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', 'MONANA'),
    imgurURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA.png`,
    imgurSizedURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_preview.png`,
    imgurSmallURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_small.png`,
    description: 'モナカードではありませんがDEXでよく利用されるトークンなので専用の画像を表示しています',
    version: '1',
    tags: '',
};

TagG.aLoadRakugakiTitles().catch(console.warn);
TagG.aLoadTreasuresList().catch(console.warn);
TagG.aLoadAnimeList().catch(console.warn);
TagG.aLoadCachedAssetData().catch(console.warn);
if (!TagG.IS_LAZYLOADAVAILABLE) myswal({
    title: (TagG.jp ? 'ご注意！' : 'CAUTION!'),
    text: (TagG.jp ? 'このブラウザではGoriFiの一部のリスト表示で件数が制限されるため、他のブラウザを使ったほうがよいです。モダンなブラウザであればだいたい大丈夫ですが、推奨はChromeです。' : 'As this browser can\'t display all items of some list in GoriFi, it is better to use another browser. A modern browser is usually fine, but Chrome is recommended.'),
    icon: 'warning',
    isCutin: true,
});
riot.tag2('markets-page', '<div class="app-page"> <div class="page-content"> <material-tabbar ref="tabbar" items="{TAB_ITEMS}" default_value="{tab}" on_change="{onChangeTab}"></material-tabbar> <div> <sales-tab if="{tab === TABS.SALES}" goto_orderbook="{gotoOrderbook}"></sales-tab> <offers-tab if="{tab === TABS.OFFERS}" goto_orderbook="{gotoOrderbook}"></offers-tab> <orderbook-tab if="{tab === TABS.ORDERBOOK}" base_asset="{asset1}"></orderbook-tab> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const TABS = this.TABS = { SALES: 'SALES', OFFERS: 'OFFERS', ORDERBOOK: 'ORDERBOOK' };
    this.TAB_ITEMS = [
        { value: TABS.SALES, label: 'SALES', icon: 'shopping_cart' },
        { value: TABS.OFFERS, label: 'OFFERS', icon: 'gavel' },
        { value: TABS.ORDERBOOK, label: 'ORDERBOOK', icon: 'list' },
    ];
    this.tab = opts.tab || TABS.ORDERBOOK;
    this.asset1 = opts.asset1;
    this.onChangeTab = (value) => {
        this.tab = value;
        switch (value) {
            case TABS.SALES:
                route('/sales');
                break;
            case TABS.OFFERS:
                route('/offers');
                break;
            case TABS.ORDERBOOK:
                route('/orderbook');
                break;
        }
        this.update();
        this.asset1 = null;
    };
    this.on('mount', () => {
        this.asset1 = null;
    });
    this.gotoOrderbook = (baseAsset) => {
        this.asset1 = baseAsset;
        this.refs.tabbar.setValue(TABS.ORDERBOOK);
        this.update();
    };
});
riot.tag2('offers-tab', '<div class="tab-frame"> <div class="search-frame"> <div class="input-frame"> <material-textfield ref="search_input" hint="Search" on_input="{onInputSearch}"></material-textfield> <div class="clearbutton-frame"><material-iconbutton icon="clear" on_click="{onClickClearSearchButton}"></material-iconbutton></div> </div><div class="chips-frame"> <div each="{searchChips}" class="chip-frame"><material-chip label="{label}" on_click="{onClick}"></material-chip></div> </div> </div> <div class="offers-frame"> <div each="{offersToShow}" class="offers-item clickable" onclick="{onClick}"> <img class="card-img bg-checker {square: assetInfo.isSquare}" riot-src="{image || noimgImg}" loading="lazy"> <div class="texts"> <div if="{!assetInfo.group}" class="name">{IS_NOTPC ? assetInfo.shortenName : assetInfo.name}</div> <div if="{assetInfo.group}" class="name">{assetInfo.group} <div class="nft-tag"></div></div> <div if="{assetInfo.group}" class="nft-id quiet">{IS_NOTPC ? assetInfo.shortenName : assetInfo.name}</div> <div class="price">{priceS}<span class="price-unit"> {TagG.quoteAsset}</span></div> </div> <div if="{isInWallet}" class="opt-icon wallet-icon"><i class="material-icons">account_balance_wallet</i></div> <div if="{treasureAmountsS}" class="opt-icon attachment-icon"><i class="material-icons">attachment</i></div> </div> </div> <util-spinner ref="spinner"></util-spinner> </div> <div ref="swalwindows_container" show="{false}"> <div ref="sell_window" class="offers-swalwindow sell-window"><div if="{offerData}"> <div class="title">Sell {offerAssetInfo.group || offerAssetInfo.name} <div if="{offerAssetInfo.group}" class="nft-tag"></div></div> <div if="{offerAssetInfo.group}" class="basic"><i class="material-icons inline-icon">tag</i> {offerAssetInfo.name}</div> <img class="asset-img bg-checker" riot-src="{offerData.imageL}" loading="lazy"> <div class="basic"><span class="quiet">Issued:</span> {commafy(offerAssetInfo.issuedAmount)} <i if="{offerAssetInfo.isLocked}" class="material-icons inline-icon">lock</i></div> <div if="{offerCard}" class="basic"><span class="quiet">Monacard:</span> {offerCard.cardName}</div> <div if="{offerAssetInfo.appData.rakugakinft}" class="basic"><span class="quiet">Rakugaki NFT:</span> {TagG.asset2rakugakiTitle[offerAssetInfo.asset] || \'No Title\'}</div> <div if="{offerData.treasureAmountsS}" class="basic"><span class="quiet">TokenVault:</span> {offerData.treasureAmountsS}</div> <div if="{offerCard}" class="basic oblique">{offerCard.description}</div> <div if="{offerAssetInfo.appData.attributes}" class="basic attrs"> <div each="{offerAssetInfo.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> <div if="{!offerCard && !offerAssetInfo.parsedDesc && offerAssetInfo.description}" class="basic oblique">{offerAssetInfo.description}</div> <div class="buttons-frame"> <material-button label="MPCHAIN" icon="search" is_link="true" link_href="{offerAssetInfo.explorerURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{offerCard}" label="Monacard" icon="open_in_new" is_link="true" link_href="{offerCard.siteURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{offerAssetInfo.appData.monacharat}" label="MonaCharaT" icon="open_in_new" is_link="true" link_href="{offerAssetInfo.appData.monacharat.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{offerAssetInfo.appData.monacute}" label="Monacute" icon="open_in_new" is_link="true" link_href="{offerAssetInfo.appData.monacute.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{offerData.treasureAmountsS}" label="TokenVault" icon="open_in_new" is_link="true" link_href="{treasuresURL(saleAssetInfo.asset)}" link_target="_blank" reload_opts="true"></material-button> <material-button label="Orderbook" icon="list" reload_opts="true" on_click="{gotoOfferAssetOrderbook}"></material-button> </div> <div class="section"> <div class="basic"> <span if="{TagG.jp}">1 枚あたり {offerData.priceS}&nbsp;{TagG.quoteAsset} で {commafy(offerData.amount)}&nbsp;枚まで売れます</span> <span if="{!TagG.jp}">You can sell up to {commafy(offerData.amount)} tokens at {offerData.priceS}&nbsp;{TagG.quoteAsset} per token.</span> </div> <div if="{!TagG.address}" class="basic">{TagG.jp ? \'売買するにはウォレットに接続してください\' : \'To trade, connect wallet.\'}</div> <div if="{TagG.address}"> <div class="basic"><i class="material-icons inline-icon">account_balance_wallet</i> {commafy(offerAssetBalance)}&nbsp;{offerAssetInfo.shortenName} / {commafy(quoteAssetBalance)}&nbsp;{TagG.quoteAsset}</div> <div class="basic"> <div class="inline-input inline-input-amount"><material-textfield ref="sell_amount" hint="Amount" on_input="{onInputSellAmount}"></material-textfield></div> <div class="text-with-inlineinput">tokens</div> </div> <div class="basic">Total sales: {commafy(sellTotalAmount)}&nbsp;{TagG.quoteAsset}</div> </div> </div> <div if="{TagG.address}" class="bottombutton-frame"><material-button label="Sell" on_click="{onClickSellButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div></div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const IS_NOTPC = this.IS_NOTPC = (document.body.clientWidth < WIDTH_PC);
    const commafy = this.commafy = Util.commafy;
    const init = () => {
        this.gotoOrderbook = opts.goto_orderbook;
        this.noimgImg = TagG.RELATIVE_URL_BASE + 'img/noimg.png';
        this.offers = [];
        this.offersToShow = [];
        this.searchedQuery = '';
        this.searchChips = [
            { label: 'in Wallet', onClick: () => search('[INWAL]') },
        ];
        if (TagG.query[TagG.QUERYKEY_SEARCH]) {
            this.searchedQuery = decodeURIComponent(TagG.query[TagG.QUERYKEY_SEARCH].replace(/\+/g, ' '));
            delete TagG.query[TagG.QUERYKEY_SEARCH];
        }
    };
    this.treasuresURL = (asset) => TagG.TREASURES_URL_FORMAT.replace('{ASSET}', asset);
    this.onInputSearch = (query) => {
        query = query.trim();
        if (query === this.searchedQuery) return;
        search(query);
    };
    this.onClickClearSearchButton = () => {
        if (this.searchedQuery === '') return;
        search('');
    };
    this.onInputSellAmount = () => {
        setSellTotalAmount();
        this.update();
    };
    this.onClickSellButton = async() => {
        const amount = Number(this.refs.sell_amount.getValue());
        if (isNaN(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量に変な値が入っています' : 'Invalid amount.', 'error');
        if (amount <= 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は正の値で入力してください' : 'Invalid amount.', 'error');
        const totalAmount = TagG.qty2amount(calcTotalSatToSell(amount), TagG.isQuoteAssetDivisible);
        if (!this.offerAssetInfo.isDivisible && !Number.isInteger(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は整数で入力してください' : 'Invalid amount.', 'error');
        if (!TagG.isQuoteAssetDivisible && !Number.isInteger(totalAmount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'Totalが整数になるように入力してください' : '"Total" must be an integer.', 'error');
        const getSat = TagG.amount2qty(totalAmount, TagG.isQuoteAssetDivisible);
        const giveSat = TagG.amount2qty(amount, this.offerAssetInfo.isDivisible);
        const success = await TagG.aMonapartyOrder(this.offerAssetInfo.asset, giveSat, TagG.quoteAsset, getSat);
        if (!success) return;
        swal.close();
    };
    this.on('mount', async() => {
        TagG.onLoadWalletManually = () => { aShowOffers() };
        if (!TagG.wallet) await TagG.aLoadWallet();
        else if (TagG.address) await TagG.aUpdateBalances();
        await aShowOffers();
    });
    const aShowOffers = async() => {
        try {
            this.refs.spinner.start();
            const params = {
                'order_by': 'block_index',
                'order_dir': 'desc',
                'status': 'open',
                'filters': [
                    { field: 'give_asset', op: '==', value: TagG.quoteAsset },
                    { field: 'get_asset', op: '!=', value: TagG.BASECHAIN_ASSET },
                ],
            };
            const orders = await Monaparty.aPartyTableAll('get_orders', params);
            const assets = orders.map(order => order.get_asset);
            await TagG.aLoadAssets(assets);
            const asset2highestOrder = {};
            for (const order of orders) {
                order.rawPrice = order.give_quantity / order.get_quantity;
                const highestOrder = asset2highestOrder[order.get_asset];
                if (!highestOrder || order.rawPrice > highestOrder.rawPrice) {
                    order.isHighest = true;
                    asset2highestOrder[order.get_asset] = order;
                    if (highestOrder) highestOrder.isHighest = false;
                }
            }
            this.offers = orders.filter(order => order.isHighest).map(order => {
                const assetInfo = TagG.asset2info[order.get_asset];
                const amount = TagG.qty2amount(order.get_remaining, assetInfo.isDivisible);
                const giveQtyN = BigInt(order.give_quantity);
                const getQtyN = BigInt(order.get_quantity);
                let priceQtyN;
                if (assetInfo.isDivisible) priceQtyN = giveQtyN * BigInt(TagG.SATOSHI_RATIO) / getQtyN;
                else priceQtyN = giveQtyN / getQtyN;
                const priceQty = Number(priceQtyN);
                const price = TagG.qty2amount(priceQty, TagG.isQuoteAssetDivisible);
                const priceS = commafy(price);
                const searchText = getSearchText(assetInfo);
                const image = TagG.getAssetImageURL('auto', assetInfo.asset);
                const imageL = TagG.getAssetImageURL('L', assetInfo.asset);
                let treasureAmountsS = null;
                if (TagG.asset2treasureQtys[assetInfo.asset]) treasureAmountsS = TagG.asset2treasureQtys[assetInfo.asset].
                    map(qty => commafy(TagG.qty2amount(qty, assetInfo.isDivisible))).join(', ');
                const isInWallet = Boolean(TagG.asset2qty[assetInfo.asset]);
                const offer = { assetInfo, price, priceS, amount, searchText, image, imageL, treasureAmountsS, isInWallet };
                offer.onClick = () => { openSellWindow(offer) };
                return offer;
            });
            search(this.searchedQuery);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。ヒント: ${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { this.refs.spinner.stop() }
    };
    const openSellWindow = (offer) => {
        this.offerData = offer;
        this.offerAssetInfo = offer.assetInfo;
        this.offerCard = TagG.asset2card[offer.assetInfo.name];
        this.quoteAssetBalance = TagG.qty2amount(TagG.asset2qty[TagG.quoteAsset] || 0, TagG.isQuoteAssetDivisible);
        this.offerAssetBalance = TagG.qty2amount(TagG.asset2qty[this.offerAssetInfo.asset] || 0, this.offerAssetInfo.isDivisible);
        this.gotoOfferAssetOrderbook = () => {
            swal.close();
            this.gotoOrderbook(this.offerAssetInfo.name);
        };
        this.update();
        if (TagG.address) {
            const defaultAmount = Math.min(offer.amount, 1);
            this.refs.sell_amount.setValue(defaultAmount);
            setSellTotalAmount();
            this.update();
        }
        Util.aOpenContentSwal(this.refs.sell_window);
    };
    const setSellTotalAmount = () => {
        const amount = Number(this.refs.sell_amount.getValue());
        if (isNaN(amount)) this.sellTotalAmount = 0;
        else this.sellTotalAmount = TagG.qty2amount(calcTotalSatToSell(amount), TagG.isQuoteAssetDivisible);
    };
    const calcTotalSatToSell = (amount) => {
        let totalSat = TagG.amount2qty(this.offerData.price * amount, TagG.isQuoteAssetDivisible);
        if (this.offerAssetInfo.isDivisible) {
            const priceSatN = BigInt(TagG.amount2qty(this.offerData.price, TagG.isQuoteAssetDivisible));
            const amountSatN = BigInt(amount * TagG.SATOSHI_RATIO);
            if (priceSatN * amountSatN < BigInt(totalSat) * BigInt(TagG.SATOSHI_RATIO)) totalSat--;
        }
        return totalSat;
    };
    const getSearchText = (assetInfo) => {
        const cardData = TagG.asset2card[assetInfo.name];
        const lockFlag = assetInfo.isLocked ? '[LOCKED]' : '[NLOCKED]';
        const tokenVaultFlag = TagG.asset2treasureQtys[assetInfo.asset] ? '[TVLT]' : '[NTVLT]';
        const inwalletFlag = TagG.asset2qty[assetInfo.asset] ? '[INWAL]' : '[NINWAL]';
        let text = `${assetInfo.name.toLowerCase()} ${assetInfo.name} ${assetInfo.asset} ${assetInfo.owner} ${assetInfo.description} ${lockFlag} ${tokenVaultFlag} ${inwalletFlag}`;
        if (assetInfo.group) text += ` ${assetInfo.group} [NFT]`;
        else text += ` [FT]`;
        if (!cardData) return text;
        text += ` ${cardData.cardName} ${cardData.ownerName} ${cardData.description} ${cardData.tags}`;
        return text;
    };
    const search = (query) => {
        this.searchedQuery = query;
        let isOR = false;
        let words = query.split(/\s+OR\s+/).map(word => word.trim()).filter(word => word);
        if (words.length > 1) isOR = true;
        else words = query.split(/\s+/).filter(word => word);
        let filteredList;
        if (words.length === 0) filteredList = this.offers;
        else {
            if (isOR) {
                filteredList = this.offers.filter(dispenser => {
                    for (const word of words) {
                        if (dispenser.searchText.includes(word)) return true;
                    }
                    return false;
                });
            }
            else {
                filteredList = this.offers.filter(dispenser => {
                    for (const word of words) {
                        if (!dispenser.searchText.includes(word)) return false;
                    }
                    return true;
                });
            }
        }
        if (this.refs.search_input && this.refs.search_input.getValue() !== query) this.refs.search_input.setValue(query);
        setURLQuery(query);
        const FIRST = 30;
        const ONCE = 300;
        this.offersToShow = filteredList.slice(0, FIRST);
        this.update();
        let length = FIRST;
        const showMore = () => {
            if (length >= filteredList.length || query !== this.searchedQuery) return;
            if (!TagG.IS_LAZYLOADAVAILABLE && length > 300) return;
            length += ONCE;
            this.offersToShow = filteredList.slice(0, length);
            this.update();
            setTimeout(showMore, 0);
        };
        setTimeout(showMore, 0);
    };
    const setURLQuery = (query) => {
        const urlObj = new URL(location);
        if (query) urlObj.searchParams.set(TagG.QUERYKEY_SEARCH, query);
        else urlObj.searchParams.delete(TagG.QUERYKEY_SEARCH);
        history.replaceState(null, '', urlObj.toString());
    };
    init();
});
riot.tag2('orderbook-tab', '<div class="tab-frame"> <div class="selectmarket-frame"> <div class="input-frame"> <material-textfield ref="baseasset_input" hint="Token" on_enter="{onEnterBaseAsset}"></material-textfield> <div class="enterbutton-frame"><material-iconbutton icon="refresh" on_click="{onEnterBaseAsset}"></material-iconbutton></div> </div><div class="recents-frame"> <div each="{base in recentBaseNames}" class="chip-frame"><material-chip reload_opts="true" label="{TagG.shortenAsset(base)}" on_click="{() => onClickBaseAsset(base)}"></material-chip></div> </div> </div> <div class="chart-frame"> <canvas ref="chart_canvas" width="400" height="406"></canvas> </div> <div class="depth-frame"> <div class="labels-row"> <div class="price-cell">Price<br>[{TagG.quoteAsset}]</div> <div class="amount-cell">Amount<br>[{baseAssetInfo.shortenName || \'---\'}]</div> </div> <div each="{ask, i in asks}" class="row ask-row {odd-row: i % 2 === 0}"> <div if="{ask.isAuctionDec}" class="price-cell">AUCTION COMING</div> <div if="{!ask.isAuctionDec}" class="price-cell"><span class="clickable" onclick="{ask.onClickPrice}">{ask.priceS1}<span if="{ask.priceS2}">.</span><span if="{ask.priceS2}" class="fracpart {fracpart-zero: ask.isPriceFracZero}"> {ask.priceS2} {ask.priceS3}</span></span></div> <div class="amount-cell"><span class="clickable" onclick="{ask.onClickAmount}">{ask.amountS1}<span if="{ask.amountS2}">.</span><span if="{ask.amountS2}" class="fracpart {fracpart-zero: ask.isAmountFracZero}"> {ask.amountS2} {ask.amountS3}</span></span></div> </div> <div class="last"> <span if="{lastPriceS && isLastBuy}" class="buy clickable" onclick="{onClickLastPrice}"><i class="material-icons icon">arrow_upward</i> {lastPriceS1}<span if="{lastPriceS2}">.</span><span if="{lastPriceS2}" class="fracpart {fracpart-zero: isLastPriceFracZero}"> {lastPriceS2} {lastPriceS3}</span></span> <span if="{lastPriceS && !isLastBuy}" class="sell clickable" onclick="{onClickLastPrice}"><i class="material-icons icon">arrow_downward</i> {lastPriceS1}<span if="{lastPriceS2}">.</span><span if="{lastPriceS2}" class="fracpart {fracpart-zero: isLastPriceFracZero}"> {lastPriceS2} {lastPriceS3}</span></span> </div> <div each="{bid, i in bids}" class="row bid-row {odd-row: i % 2 === 0}"> <div class="price-cell"><span class="clickable" onclick="{bid.onClickPrice}">{bid.priceS1}<span if="{bid.priceS2}">.</span><span if="{bid.priceS2}" class="fracpart {fracpart-zero: bid.isPriceFracZero}"> {bid.priceS2} {bid.priceS3}</span></span></div> <div class="amount-cell"><span class="clickable" onclick="{bid.onClickAmount}">{bid.amountS1}<span if="{bid.amountS2}">.</span><span if="{bid.amountS2}" class="fracpart {fracpart-zero: bid.isAmountFracZero}"> {bid.amountS2} {bid.amountS3}</span></span></div> </div> </div> <div class="orderform-frame"> <div class="buysellswitch-frame"> <div class="buysellswitch-button {selected: isFormBuy, selected-buy: isFormBuy}"><material-button label="Buy" on_click="{onClickBuySwitchButton}" is_white="true" is_fullwidth="true"></material-button></div><div class="buysellswitch-button {selected: !isFormBuy, selected-sell: !isFormBuy}"><material-button label="Sell" on_click="{onClickSellSwitchButton}" is_white="true" is_fullwidth="true"></material-button></div> </div> <div class="margin-frame"><i class="material-icons wallet-icon">account_balance_wallet</i> {commafy(baseAssetBalance)}&nbsp;{baseAssetInfo.shortenName || \'---\'} / {commafy(quoteAssetBalance)}&nbsp;{TagG.quoteAsset}</div> <div class="margin-frame"> <material-textfield ref="price_input" hint="Price" on_input="{onChangePriceInput}"></material-textfield> <div if="{!isFormBuy}" class="auction-button"><material-iconbutton icon="gavel" on_click="{onClickAuctionButton}"></material-iconbutton></div> </div> <div class="margin-frame"> <div><material-textfield ref="amount_input" hint="Amount" on_input="{onChangeAmountInput}"></material-textfield></div> <div class="max-amount quiet"><span class="clickable" onclick="{onClickMaxAmount}">Max: {commafy(maxAmount)}</span></div> </div> <div class="margin-frame">Total: {commafy(totalAmount)} {TagG.quoteAsset}</div> <div> <div if="{isFormBuy}" class="buysell-button-frame {buy-button-frame: isOrderable}"><material-button ref="order_button" label="BUY" on_click="{onClickOrderButton}" is_white="true" is_fullwidth="true" is_disabled="true"></material-button></div> <div if="{!isFormBuy}" class="buysell-button-frame {sell-button-frame: isOrderable}"><material-button ref="order_button" label="SELL" on_click="{onClickOrderButton}" is_white="true" is_fullwidth="true" is_disabled="true"></material-button></div> </div> <div if="{!TagG.address}" class="orderform-nologin">{TagG.jp ? \'売買するにはウォレットに接続してください\' : \'To trade, connect wallet.\'}</div> <div if="{TagG.address && !baseAssetInfo.name}" class="orderform-nologin"></div> </div> <div class="assetinfo-frame {assetinfo-frame-notpc: IS_NOTPC}"> <div class="card"><img class="card-img bg-checker" riot-src="{baseAssetImage || TagG.RELATIVE_URL_BASE + \'img/noimg.png\'}" loading="lazy"></div> <div if="{!IS_NOTPC}" class="text"> <div if="{!baseAssetInfo.name}" class="basic">{TagG.jp ? \'上の入力欄にトレードしたいトークンを入力してください\' : \'Enter the name of token you want to trade in the input box above.\'}</div> <div if="{baseAssetInfo.name}"> <div class="name">{baseAssetInfo.name}</div> <div if="{baseAssetInfo.group}" class="basic"><span class="quiet">NFT Group:</span> {baseAssetInfo.group}</div> <div if="{!baseAssetInfo.parsedDesc && baseAssetInfo.description}" class="basic">{baseAssetInfo.description}</div> <div class="basic"><span class="quiet">Issued:</span> {commafy(baseAssetInfo.issuedAmount)} <i if="{baseAssetInfo.isLocked}" class="material-icons inline-icon">lock</i></div> <div if="{baseAssetInfo.appData?.attributes}" class="basic attrs"> <div each="{baseAssetInfo.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> <div class="button-frame"><material-button label="MPCHAIN" icon="search" is_link="true" link_href="{baseAssetInfo.explorerURL}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> </div> <div if="{baseCard}" class="additionals"> <div class="cardname">{baseCard.cardName}</div> <div class="basic"><span class="quiet">by</span> {baseCard.ownerName} <a if="{baseCard.ownerSN}" href="{twitterURL(baseCard.ownerSN)}" target="_blank">@{baseCard.ownerSN}</a></div> <div class="basic">{baseCard.description}</div> <div class="button-frame"><material-button label="Monacard" icon="open_in_new" is_link="true" link_href="{baseCard.siteURL}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> </div> <div if="{baseAssetInfo.appData?.monacharat}" class="additionals"> <div class="basic quiet">MonaCharaT</div> <div class="button-frame"><material-button label="MonaCharaT" icon="open_in_new" is_link="true" link_href="{baseAssetInfo.appData.monacharat.url}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> </div> <div if="{baseAssetInfo.appData?.rakugakinft}" class="additionals"> <div class="basic quiet">Rakugaki NFT</div> <div class="cardname">{TagG.asset2rakugakiTitle[baseAssetInfo.asset] || \'No Title\'}</div> </div> <div if="{baseAssetInfo.appData?.monacute}" class="additionals"> <div class="basic quiet">Monacute</div> <div class="button-frame"><material-button label="Monacute" icon="open_in_new" is_link="true" link_href="{baseAssetInfo.appData.monacute.url}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> </div> <div if="{baseTreasureAmountsS}" class="additionals"> <div class="basic"><span class="quiet">TokenVault:</span> {baseTreasureAmountsS}</div> <div class="button-frame"><material-button label="TokenVault" icon="open_in_new" is_link="true" link_href="{treasuresURL(baseAssetInfo.asset)}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> </div> </div> <div if="{IS_NOTPC}" class="text"> <div if="{!baseAssetInfo.name}" class="basic">{TagG.jp ? \'上の入力欄にトレードしたいトークンを入力してください\' : \'Enter the name of token you want to trade in the input box above.\'}</div> <div if="{baseAssetInfo.name}"> <div class="name">{baseAssetInfo.name}</div> <div if="{baseAssetInfo.group}" class="basic"><span class="quiet">NFT Group:</span> {baseAssetInfo.group}</div> <div if="{!baseAssetInfo.parsedDesc && baseAssetInfo.description}" class="basic">{baseAssetInfo.description}</div> <div class="basic"><span class="quiet">Issued:</span> {commafy(baseAssetInfo.issuedAmount)} <i if="{baseAssetInfo.isLocked}" class="material-icons inline-icon">lock</i></div> <div if="{baseCard}" class="basic"><span class="quiet">Monacard:</span> {baseCard.cardName}</div> <div if="{baseAssetInfo.appData?.rakugakinft}" class="basic"><span class="quiet">Rakugaki NFT:</span> {TagG.asset2rakugakiTitle[baseAssetInfo.asset] || \'No Title\'}</div> <div if="{baseTreasureAmountsS}" class="basic"><span class="quiet">TokenVault:</span> {baseTreasureAmountsS}</div> <div if="{baseAssetInfo.appData?.attributes}" class="basic attrs"> <div each="{baseAssetInfo.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> <div class="button-frame"><material-button label="MPCHAIN" icon="search" is_link="true" link_href="{baseAssetInfo.explorerURL}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> <div if="{baseCard}" class="button-frame"><material-button label="Monacard" icon="open_in_new" is_link="true" link_href="{baseCard.siteURL}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> <div if="{baseAssetInfo.appData?.monacharat}" class="button-frame"><material-button label="MonaCharaT" icon="open_in_new" is_link="true" link_href="{baseAssetInfo.appData.monacharat.url}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> <div if="{baseAssetInfo.appData?.monacute}" class="button-frame"><material-button label="Monacute" icon="open_in_new" is_link="true" link_href="{baseAssetInfo.appData.monacute.url}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> <div if="{baseTreasureAmountsS}" class="button-frame"><material-button label="TokenVault" icon="open_in_new" is_link="true" link_href="{treasuresURL(baseAssetInfo.asset)}" link_target="_blank" is_fullwidth="true" reload_opts="true"></material-button></div> </div> </div> </div> <div class="myorders-frame"> <div class="myorders-tabbar"> <material-tabbar items="{MYORDER_TABITEMS}" default_value="{myOrdersTab}" on_change="{onChangeMyOrdersTab}"></material-tabbar> <div class="update-button"><material-iconbutton icon="refresh" on_click="{onClickMyOrdersUpdateButton}"></material-iconbutton></div> </div> <div if="{myOrdersTab === MYORDER_TABS.OPEN}"> <div class="labels-row quiet"> <div class="cell open-asset-cell">Token</div> <div class="cell open-side-cell">Side</div> <div class="cell open-price-cell">Price</div> <div class="cell open-amount-cell">Filled&nbsp;amount / Order&nbsp;amount</div> <div class="cell open-cancel-cell"></div> </div> <div each="{o, i in openOrders}" class="row {even-row: i % 2 === 1}"> <div class="cell open-asset-cell"><span class="clickable" onclick="{() => onClickBaseAsset(o.assetName)}">{TagG.shortenAsset(o.assetName)}</span></div> <div class="cell open-side-cell {buy: o.isBuy, sell: !o.isBuy}">{o.side}</div> <div class="cell open-price-cell">{commafy(o.price)}</div> <div class="cell open-amount-cell">{commafy(o.filledAmount)} / {commafy(o.orderAmount)}</div> <div class="cell open-cancel-cell"><material-button label="Cancel" is_fullwidth="true" reload_opts="true" on_click="{o.onClickCancelButton}"></material-button></div> </div> <div if="{myOrdersLoadFailed}" class="message">Failed to load</div> <div if="{openOrders.length === 0}" class="message">No open orders</div> </div> <div if="{myOrdersTab === MYORDER_TABS.HISTORY}"> <div class="labels-row quiet"> <div class="cell history-asset-cell">Token</div> <div class="cell history-side-cell">Side</div> <div class="cell history-price-cell">Price</div> <div class="cell history-amount-cell">Filled&nbsp;amount / Order&nbsp;amount</div> <div class="cell history-status-cell">Status</div> </div> <div each="{h, i in orderHistory}" class="row {even-row: i % 2 === 1}"> <div class="cell history-asset-cell"><span class="clickable" onclick="{() => onClickBaseAsset(h.assetName)}">{TagG.shortenAsset(h.assetName)}</span></div> <div class="cell history-side-cell {buy: h.isBuy, sell: !h.isBuy}">{h.side}</div> <div class="cell history-price-cell">{commafy(h.price)}</div> <div class="cell history-amount-cell">{commafy(h.filledAmount)} / {commafy(h.orderAmount)}</div> <div class="cell history-status-cell">{h.status}</div> </div> <div if="{myOrdersLoadFailed}" class="message">Failed to load</div> <div if="{orderHistory.length === 0}" class="message">No order history</div> </div> <util-spinner ref="myorders_spinner"></util-spinner> </div> <div if="{auctions.length}" class="auctions-frame"> <div class="title"><i class="material-icons inline-icon">gavel</i> Auctions</div> <div class="auction-items"> <div each="{auctions}" class="auction-item clickable" onclick="{() => onClickBaseAsset(assetName)}"> <img if="{image}" class="image bg-checker" riot-src="{image}" loading="lazy"> <div if="{!image}" class="label">{assetName}</div> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const IS_NOTPC = this.IS_NOTPC = (document.body.clientWidth < WIDTH_PC);
    const RECENT_BASEASSETS = 20;
    const MIN_DEPTH_ROWS = IS_NOTPC ? 4 : 8;
    const MAX_DEPTH_ROWS = IS_NOTPC ? 4 : 20;
    const MYORDER_TABS = this.MYORDER_TABS = { OPEN: 'OPEN', HISTORY: 'HISTORY' };
    const defaultCommafy = this.commafy = Util.commafy;
    const decimalCommafy = (num) => Util.commafy(num, { decimal: TagG.SATOSHI_DECIMAL, removeDecimalZero: false });
    const init = () => {
        this.MYORDER_TABITEMS = [
            { value: MYORDER_TABS.OPEN, label: 'Open Orders' },
            { value: MYORDER_TABS.HISTORY, label: 'Order History' },
        ];
        this.recentBaseNames = Util.loadStorage(TagG.STORAGEKEY_RECENTBASES) || [];
        this.initBaseAsset = opts.base_asset;
        this.baseAssetInfo = {};
        this.asks = [];
        this.bids = [];
        this.openOrders = [];
        this.orderHistory = [];
        this.auctions = [];
        while (this.asks.length < MIN_DEPTH_ROWS) this.asks.unshift({});
        while (this.bids.length < MIN_DEPTH_ROWS) this.bids.push({});
        this.isFormBuy = true;
        this.baseAssetBalance = 0;
        this.quoteAssetBalance = 0;
        this.totalAmount = 0;
        this.myOrdersTab = MYORDER_TABS.OPEN;
    };
    this.twitterURL = (userSN) => TagG.TWITTER_USERURL_FORMAT.replace('{SN}', userSN);
    this.treasuresURL = (asset) => TagG.TREASURES_URL_FORMAT.replace('{ASSET}', asset);
    this.onEnterBaseAsset = () => {
        const inputAssetName = this.refs.baseasset_input.getValue().trim();
        if (!inputAssetName) return;
        aShowMarket(inputAssetName);
    };
    this.onClickBaseAsset = (baseName) => {
        aShowMarket(baseName);
        this.refs.baseasset_input.focus();
        try { scrollTo({ left: 0, top: 0, behavior: 'smooth' }) }
        catch (error) { scrollTo(0, 0) }
    };
    this.onClickLastPrice = () => { if (this.lastPriceS) setPrice(this.lastPrice) };
    this.onClickAuctionButton = () => { setPrice(TagG.AUCTIONFLAG_PRICE) };
    this.onClickMaxAmount = () => { setAmount(this.maxAmount) };
    this.onChangePriceInput = () => { onChangeOrderInputs() };
    this.onChangeAmountInput = () => { onChangeOrderInputs() };
    this.onClickBuySwitchButton = () => {
        this.isFormBuy = true;
        this.update();
        onChangeOrderInputs();
    };
    this.onClickSellSwitchButton = () => {
        this.isFormBuy = false;
        this.update();
        onChangeOrderInputs();
    };
    this.onClickOrderButton = async() => {
        const price = Number(this.refs.price_input.getValue());
        const amount = Number(this.refs.amount_input.getValue());
        if (isNaN(price)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '価格に変な値が入っています' : 'Invalid price.', 'error');
        if (isNaN(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量に変な値が入っています' : 'Invalid amount.', 'error');
        if (price <= 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '価格は正の値で入力してください' : 'Invalid price.', 'error');
        if (amount <= 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は正の値で入力してください' : 'Invalid amount.', 'error');
        const totalAmount = price * amount;
        if (!this.baseAssetInfo.isDivisible && !Number.isInteger(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は整数で入力してください' : 'Invalid amount.', 'error');
        if (!TagG.isQuoteAssetDivisible && !Number.isInteger(totalAmount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'Totalが整数になるように入力してください' : '"Total" must be an integer.', 'error');
        let success;
        if (this.isFormBuy) {
            const giveSat = TagG.amount2qty(totalAmount, TagG.isQuoteAssetDivisible);
            const getSat = TagG.amount2qty(amount, this.baseAssetInfo.isDivisible);
            success = await TagG.aMonapartyOrder(TagG.quoteAsset, giveSat, this.baseAssetInfo.asset, getSat);
        }
        else {
            const giveSat = TagG.amount2qty(amount, this.baseAssetInfo.isDivisible);
            const getSat = TagG.amount2qty(totalAmount, TagG.isQuoteAssetDivisible);
            success = await TagG.aMonapartyOrder(this.baseAssetInfo.asset, giveSat, TagG.quoteAsset, getSat);
        }
        if (!success) return;
        this.refs.amount_input.setValue('');
        onChangeOrderInputs();
    };
    this.onChangeMyOrdersTab = (value) => {
        this.myOrdersTab = value;
        this.update();
    };
    this.onClickMyOrdersUpdateButton = () => { aShowMyOrders() };
    this.on('mount', async() => {
        createChart();
        TagG.onLoadWalletManually = () => {
            aShowMyOrders();
            if (this.baseAssetInfo.name) aShowMarket(this.baseAssetInfo.name);
        };
        if (!TagG.wallet) await TagG.aLoadWallet();
        else if (TagG.address) await TagG.aUpdateBalances();
        if (TagG.address) aShowMyOrders();
        aShowAuctions();
        if (this.initBaseAsset) await aShowMarket(this.initBaseAsset);
        await Util.aSleep(100);
        if (this.refs.baseasset_input) this.refs.baseasset_input.focus();
    });
    const onChangeOrderInputs = () => {
        const price = Number(this.refs.price_input.getValue());
        const amount = Number(this.refs.amount_input.getValue());
        if (isNaN(price) || isNaN(amount)) this.totalAmount = 0;
        else this.totalAmount = Util.round(price * amount, TagG.SATOSHI_DECIMAL);
        if (TagG.address && this.totalAmount > 0) {
            this.isOrderable = true;
            this.refs.order_button.setDisabled(false);
        }
        else {
            this.isOrderable = false;
            this.refs.order_button.setDisabled(true);
        }
        setBaseQuoteBalances();
        setMaxAmount();
        this.update();
    };
    const aShowMarket = async(inputAssetName) => {
        try {
            if (inputAssetName === TagG.quoteAsset) return;
            if (inputAssetName === TagG.BASECHAIN_ASSET) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? `${inputAssetName}はDEXでは取扱えません` : `${inputAssetName} is not available at DEX.`, 'error');
            TagG.bodySpinner.start();
            await TagG.aLoadAssets([inputAssetName]);
            const baseAssetInfo = TagG.asset2info[inputAssetName];
            if (!baseAssetInfo) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? `${inputAssetName}というトークンはありません` : `There is no token named ${inputAssetName}`, 'error');
            this.baseAssetInfo = baseAssetInfo;
            this.baseCard = TagG.asset2card[baseAssetInfo.name];
            await Promise.all([aShowDepth(), aShowChart()]);
            setBaseQuoteBalances();
            setMaxAmount();
            addRecentBase();
            this.baseAssetImage = TagG.getAssetImageURL('L', this.baseAssetInfo.asset);
            this.baseTreasureAmountsS = null;
            if (TagG.asset2treasureQtys[baseAssetInfo.asset]) this.baseTreasureAmountsS = TagG.asset2treasureQtys[baseAssetInfo.asset].
                map(qty => defaultCommafy(TagG.qty2amount(qty, baseAssetInfo.isDivisible))).join(', ');
            this.update();
            this.refs.baseasset_input.setValue(inputAssetName);
            route(`/orderbook/${encodeURIComponent(this.baseAssetInfo.name)}`);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。ヒント: ${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aShowDepth = async() => {
        const marketParams = { 'asset1': this.baseAssetInfo.asset, 'asset2': TagG.quoteAsset };
        const marketDetail = await Monaparty.aBlock('get_market_details', marketParams);
        this.asks = marketDetail.sell_orders
            .map(order => parseDepthOrder(order, marketDetail.base_asset_divisible, marketDetail.quote_asset_divisible))
            .sort(sortByPriceDesc)
            .slice(-MAX_DEPTH_ROWS);
        this.bids = marketDetail.buy_orders
            .map(order => parseDepthOrder(order, marketDetail.base_asset_divisible, marketDetail.quote_asset_divisible))
            .sort(sortByPriceDesc)
            .slice(0, MAX_DEPTH_ROWS);
        while (this.asks.length < MIN_DEPTH_ROWS) this.asks.unshift({});
        while (this.bids.length < MIN_DEPTH_ROWS) this.bids.push({});
        if (this.asks.length % 2) this.asks.unshift({});
        if (this.bids.length % 2) this.bids.push({});
        if (marketDetail.last_trades.length === 0) this.lastPriceS = null;
        else {
            const lastTrade = marketDetail.last_trades.reduce((lastTrade, trade) => (trade.block_time > lastTrade.block_time ? trade : lastTrade));
            this.lastPrice = lastTrade.price;
            if (marketDetail.quote_asset_divisible) {
                this.lastPriceS = decimalCommafy(lastTrade.price);
                this.lastPriceS1 = this.lastPriceS.slice(0, -9);
                this.lastPriceS2 = this.lastPriceS.slice(-8, -4);
                this.lastPriceS3 = this.lastPriceS.slice(-4);
                this.isLastPriceFracZero = (this.lastPriceS2 === '0000' && this.lastPriceS3 === '0000');
            }
            else {
                this.lastPriceS = defaultCommafy(lastTrade.price);
                this.lastPriceS1 = this.lastPriceS;
                this.lastPriceS2 = this.lastPriceS3 = null;
            }
            this.isLastBuy = (lastTrade.type === 'BUY');
        }
    };
    const aShowChart = async() => {
        const priceHistoryParams = { 'asset1': this.baseAssetInfo.asset, 'asset2': TagG.quoteAsset, 'as_dict': true };
        const priceHistory = await Monaparty.aBlock('get_market_price_history', priceHistoryParams) || [];
        const priceData = priceHistory.map(tick => ({ t: new Date(tick.interval_time), y: tick.close }));
        const volumeData = priceHistory.map(tick => ({ t: new Date(tick.interval_time), y: tick.vol }));
        const volumeMin = priceHistory.reduce((min, tick) => (tick.vol < min ? tick.vol : min), Number.POSITIVE_INFINITY);
        const volumeMax = priceHistory.reduce((max, tick) => (tick.vol > max ? tick.vol : max), 0);
        const volumeBase = Math.sqrt((volumeMin + volumeMax) / 2);
        const volumeSizes = priceHistory.map(tick => (1 + Math.sqrt(tick.vol) / volumeBase * 10));
        this.chart.data.datasets[0].data = priceData;
        this.chart.data.datasets[1].data = volumeData;
        this.chart.data.datasets[1].pointRadius = volumeSizes;
        this.chart.update();
    };
    const aShowMyOrders = async() => {
        if (!TagG.address) return;
        try {
            this.refs.myorders_spinner.start();
            const params = {
                'filters': [{ field: 'source', op: '==', value: TagG.address }],
                'order_by': 'block_index',
                'order_dir': 'desc',
            };
            const myOrders = await Monaparty.aParty('get_orders', params);
            const assets = [];
            for (const order of myOrders) assets.push(order.give_asset, order.get_asset);
            await TagG.aLoadAssets(assets);
            this.orderHistory = myOrders.
                filter(order => (order.give_asset === TagG.quoteAsset || order.get_asset === TagG.quoteAsset)).
                map(order => parseMyOrder(order));
            this.openOrders = this.orderHistory.filter(order => order.status === 'open');
            this.update();
        }
        catch (error) {
            console.error(error);
            this.myOrdersLoadFailed = true;
            this.update();
        }
        finally { this.refs.myorders_spinner.stop() }
    };
    const aShowAuctions = async() => {
        try {
            const params = {
                'order_by': 'block_index',
                'order_dir': 'desc',
                'status': 'open',
                'filters': [
                    { field: 'get_asset', op: '==', value: TagG.quoteAsset },
                    { field: 'give_asset', op: '!=', value: TagG.BASECHAIN_ASSET },
                    { field: 'get_quantity', op: '>=', value: TagG.amount2qty(TagG.AUCTIONFLAG_PRICE, TagG.isQuoteAssetDivisible) },
                ],
            };
            const auctionOrders = await Monaparty.aParty('get_orders', params);
            const assets = auctionOrders.map(order => order.give_asset);
            await TagG.aLoadAssets(assets);
            const asset2is = {};
            this.auctions = auctionOrders.filter(order => {
                if (asset2is[order.give_asset]) return false;
                const assetInfo = TagG.asset2info[order.give_asset];
                const getAmount = TagG.qty2amount(order.get_quantity, TagG.isQuoteAssetDivisible)
                const giveAmount = TagG.qty2amount(order.give_quantity, assetInfo.isDivisible);
                const price = Math.round(getAmount / giveAmount);
                if (price !== TagG.AUCTIONFLAG_PRICE) return false;
                asset2is[order.give_asset] = true;
                return true;
            }).map(order => parseAuctionOrders(order));
            this.update();
        }
        catch (error) { console.error(error) }
    };
    const createChart = () => {
        Chart.defaults.global.defaultColor = 'rgba(255, 255, 255, 0.8)';
        Chart.defaults.global.defaultFontColor = 'rgba(255, 255, 255, 0.8)';
        Chart.defaults.global.defaultFontFamily = "'M PLUS Rounded 1c', sans-serif";
        const context = this.refs.chart_canvas.getContext('2d');
        this.chart = new Chart(context, {
            data: {
                datasets: [{
                    label: 'Price',
                    type: 'line',
                    backgroundColor: 'rgba(255, 140, 0, 0.4)',
                    borderColor: 'rgba(255, 140, 0, 1.0)',
                    borderJoinStyle: 'round',
                    lineTension: 0,
                    fill: false,
                    yAxisID: 'price_axis',
                    data: [],
                }, {
                    label: 'Volume',
                    type: 'line',
                    backgroundColor: 'rgba(140, 255, 0, 0.4)',
                    borderColor: 'rgba(140, 255, 0, 1.0)',
                    pointRadius: [],
                    lineTension: 0,
                    fill: false,
                    showLine: false,
                    yAxisID: 'volume_axis',
                    data: [],
                }],
            },
            options: {
                maintainAspectRatio: false,
                legend: { onClick: null },
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        time: {
                            minUnit: 'hour',
                            displayFormats: {
                                hour: 'MM/DD HH:mm',
                                day: 'MM/DD',
                                week: 'MM/DD',
                                month: 'YYYY/MM/DD',
                            },
                        },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.1)',
                            zeroLineColor: 'rgba(255, 255, 255, 0.1)',
                        },
                    }],
                    yAxes: [{
                        id: 'price_axis',
                        position: 'right',
                        ticks: { beginAtZero: true },
                        gridLines: {
                            color: 'rgba(255, 255, 255, 0.1)',
                            zeroLineColor: 'rgba(255, 255, 255, 0.1)',
                        },
                    }, {
                        id: 'volume_axis',
                        display: false,
                        ticks: { beginAtZero: true },
                        gridLines: {
                            drawOnChartArea: false,
                            color: 'rgba(255, 255, 255, 0.1)',
                            zeroLineColor: 'rgba(255, 255, 255, 0.1)',
                        },
                    }],
                }
            }
        });
    };
    const setBaseQuoteBalances = () => {
        if (!TagG.address) return;
        this.baseAssetBalance = TagG.qty2amount(TagG.asset2qty[this.baseAssetInfo.asset] || 0, this.baseAssetInfo.isDivisible);
        this.quoteAssetBalance = TagG.qty2amount(TagG.asset2qty[TagG.quoteAsset] || 0, TagG.isQuoteAssetDivisible);
    };
    const setMaxAmount = () => {
        if (this.isFormBuy) {
            const price = Number(this.refs.price_input.getValue());
            if (price > 0) this.maxAmount = this.baseAssetInfo.isDivisible?
                Math.floor(this.quoteAssetBalance / price * TagG.SATOSHI_RATIO) / TagG.SATOSHI_RATIO:
                Math.floor(this.quoteAssetBalance / price);
            else this.maxAmount = 0;
        }
        else this.maxAmount = this.baseAssetBalance;
    };
    const addRecentBase = () => {
        const baseName = this.baseAssetInfo.name;
        const index = this.recentBaseNames.findIndex(base => (base === baseName));
        if (index >= 0) this.recentBaseNames.splice(index, 1);
        this.recentBaseNames.unshift(baseName);
        this.recentBaseNames = this.recentBaseNames.slice(0, RECENT_BASEASSETS);
        Util.saveStorage(TagG.STORAGEKEY_RECENTBASES, this.recentBaseNames);
    };
    const parseDepthOrder = (order, baseDivisible, quoteDivisible) => {
        const price = Number(order.price);
        const amount = TagG.qty2amount(order.amount, baseDivisible);
        let priceS1, priceS2, priceS3, amountS1, amountS2, amountS3, isPriceFracZero, isAmountFracZero;
        if (quoteDivisible) {
            const priceS = decimalCommafy(price);
            priceS1 = priceS.slice(0, -9);
            priceS2 = priceS.slice(-8, -4);
            priceS3 = priceS.slice(-4);
            isPriceFracZero = (priceS2 === '0000' && priceS3 === '0000');
        }
        else priceS1 = defaultCommafy(price);
        if (baseDivisible) {
            const amountS = decimalCommafy(amount);
            amountS1 = amountS.slice(0, -9);
            amountS2 = amountS.slice(-8, -4);
            amountS3 = amountS.slice(-4);
            isAmountFracZero = (amountS2 === '0000' && amountS3 === '0000');
        }
        else amountS1 = defaultCommafy(amount);
        const isAuctionDec = (price === TagG.AUCTIONFLAG_PRICE);
        return {
            price, amount, priceS1, priceS2, priceS3, amountS1, amountS2, amountS3,
            isPriceFracZero, isAmountFracZero, isAuctionDec,
            onClickPrice: () => setPrice(price),
            onClickAmount: () => setAmount(amount),
        };
    };
    const parseMyOrder = (order) => {
        const isBuy = (order.give_asset === TagG.quoteAsset);
        let assetInfo, side, filledAmount, orderAmount, price;
        if (isBuy) {
            assetInfo = TagG.asset2info[order.get_asset];
            side = 'Buy';
            filledAmount = TagG.qty2amount(order.get_quantity - order.get_remaining, assetInfo.isDivisible);
            orderAmount = TagG.qty2amount(order.get_quantity, assetInfo.isDivisible);
            const orderTotal = TagG.qty2amount(order.give_quantity, TagG.isQuoteAssetDivisible);
            price = Util.round(orderTotal / orderAmount, TagG.SATOSHI_DECIMAL);
        }
        else {
            assetInfo = TagG.asset2info[order.give_asset];
            side = 'Sell';
            filledAmount = TagG.qty2amount(order.give_quantity - order.give_remaining, assetInfo.isDivisible);
            orderAmount = TagG.qty2amount(order.give_quantity, assetInfo.isDivisible);
            const orderTotal = TagG.qty2amount(order.get_quantity, TagG.isQuoteAssetDivisible);
            price = Util.round(orderTotal / orderAmount, TagG.SATOSHI_DECIMAL);
        }
        return {
            isBuy, side, price, filledAmount, orderAmount,
            assetName: assetInfo.name,
            status: order.status,
            onClickCancelButton: () => TagG.aMonapartyCancel(order.tx_hash),
        };
    };
    const parseAuctionOrders = (order) => {
        const assetInfo = TagG.asset2info[order.give_asset];
        return {
            assetName: assetInfo.name,
            image: TagG.getAssetImageURL('M', assetInfo.asset),
        };
    };
    const setPrice = (price) => {
        const noExpPrice = defaultCommafy(price).replace(/,/g, '');
        this.refs.price_input.setValue(noExpPrice);
        onChangeOrderInputs();
    };
    const setAmount = (amount) => {
        const noExpAmount = defaultCommafy(amount).replace(/,/g, '');
        this.refs.amount_input.setValue(noExpAmount);
        onChangeOrderInputs();
    };
    const sortByPriceDesc = (a, b) => (a.price < b.price ? 1 : -1);
    init();
});
riot.tag2('sales-tab', '<div class="tab-frame"> <div class="search-frame"> <div class="input-frame"> <material-textfield ref="search_input" hint="Search" on_input="{onInputSearch}"></material-textfield> <div class="clearbutton-frame"><material-iconbutton icon="clear" on_click="{onClickClearSearchButton}"></material-iconbutton></div> </div><div class="chips-frame"> <div each="{searchChips}" class="chip-frame"><material-chip label="{label}" on_click="{onClick}"></material-chip></div> </div> </div> <div class="sales-frame"> <div each="{salesToShow}" class="sales-item clickable" onclick="{onClick}"> <img class="card-img bg-checker {square: assetInfo.isSquare}" riot-src="{image || noimgImg}" loading="lazy"> <div class="texts"> <div if="{!assetInfo.group}" class="name">{IS_NOTPC ? assetInfo.shortenName : assetInfo.name}</div> <div if="{assetInfo.group}" class="name">{assetInfo.group} <div class="nft-tag"></div></div> <div if="{assetInfo.group}" class="nft-id quiet">{IS_NOTPC ? assetInfo.shortenName : assetInfo.name}</div> <div if="{!isAuction}" class="price">{priceS}<span class="price-unit"> {TagG.quoteAsset}</span></div> <div if="{isAuction}" class="price">Auction Coming <i class="material-icons">gavel</i></div> </div> <div if="{isInWallet}" class="opt-icon wallet-icon"><i class="material-icons">account_balance_wallet</i></div> <div if="{treasureAmountsS}" class="opt-icon attachment-icon"><i class="material-icons">attachment</i></div> </div> </div> <util-spinner ref="spinner"></util-spinner> </div> <div ref="swalwindows_container" show="{false}"> <div ref="buy_window" class="sales-swalwindow buy-window"><div if="{saleData}"> <div class="title"><span if="{!saleData.isAuction}">Buy</span><i if="{saleData.isAuction}" class="material-icons inline-icon">gavel</i> {saleAssetInfo.group || saleAssetInfo.name} <div if="{saleAssetInfo.group}" class="nft-tag"></div></div> <div if="{saleAssetInfo.group}" class="basic"><i class="material-icons inline-icon">tag</i> {saleAssetInfo.name}</div> <img class="asset-img bg-checker" riot-src="{saleData.imageL}" loading="lazy"> <div class="basic"><span class="quiet">Issued:</span> {commafy(saleAssetInfo.issuedAmount)} <i if="{saleAssetInfo.isLocked}" class="material-icons inline-icon">lock</i></div> <div if="{saleCard}" class="basic"><span class="quiet">Monacard:</span> {saleCard.cardName}</div> <div if="{saleAssetInfo.appData.rakugakinft}" class="basic"><span class="quiet">Rakugaki NFT:</span> {TagG.asset2rakugakiTitle[saleAssetInfo.asset] || \'No Title\'}</div> <div if="{saleData.treasureAmountsS}" class="basic"><span class="quiet">TokenVault:</span> {saleData.treasureAmountsS}</div> <div if="{saleCard}" class="basic oblique">{saleCard.description}</div> <div if="{!saleCard && !saleAssetInfo.parsedDesc && saleAssetInfo.description}" class="basic oblique">{saleAssetInfo.description}</div> <div if="{saleAssetInfo.appData.attributes}" class="basic attrs"> <div each="{saleAssetInfo.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> <div class="buttons-frame"> <material-button label="MPCHAIN" icon="search" is_link="true" link_href="{saleAssetInfo.explorerURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{saleCard}" label="Monacard" icon="open_in_new" is_link="true" link_href="{saleCard.siteURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{saleAssetInfo.appData.monacharat}" label="MonaCharaT" icon="open_in_new" is_link="true" link_href="{saleAssetInfo.appData.monacharat.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{saleAssetInfo.appData.monacute}" label="Monacute" icon="open_in_new" is_link="true" link_href="{saleAssetInfo.appData.monacute.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{saleData.treasureAmountsS}" label="TokenVault" icon="open_in_new" is_link="true" link_href="{treasuresURL(saleAssetInfo.asset)}" link_target="_blank" reload_opts="true"></material-button> <material-button label="Orderbook" icon="list" reload_opts="true" on_click="{gotoSaleAssetOrderbook}"></material-button> </div> <div if="{!saleData.isAuction}" class="section"> <div class="basic"> <span if="{TagG.jp}">1 枚あたり {saleData.priceS}&nbsp;{TagG.quoteAsset} で {commafy(saleData.amount)}&nbsp;枚まで買えます</span> <span if="{!TagG.jp}">You can buy up to {commafy(saleData.amount)} tokens at {saleData.priceS}&nbsp;{TagG.quoteAsset} per token.</span> </div> <div if="{!TagG.address}" class="basic">{TagG.jp ? \'売買するにはウォレットに接続してください\' : \'To trade, connect wallet.\'}</div> <div if="{TagG.address}"> <div class="basic"><i class="material-icons inline-icon">account_balance_wallet</i> {commafy(saleAssetBalance)}&nbsp;{saleAssetInfo.shortenName} / {commafy(quoteAssetBalance)}&nbsp;{TagG.quoteAsset}</div> <div class="basic"> <div class="inline-input inline-input-amount"><material-textfield ref="buy_amount" hint="Amount" on_input="{onInputBuyAmount}"></material-textfield></div> <div class="text-with-inlineinput">tokens</div> </div> <div class="basic">Total cost: {commafy(buyTotalAmount)}&nbsp;{TagG.quoteAsset}</div> <div class="bottombutton-frame"><material-button label="BUY" on_click="{onClickBuyButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> </div> <div if="{saleData.isAuction}" class="section"> <div class="basic">{saleAssetInfo.name} x {commafy(saleData.amount)}</div> <div class="basic"> <span if="{TagG.jp}">オークションの開催が予告されています。Orderbookに買注文を入れて参加しましょう</span> <span if="{!TagG.jp}">An auction will be held. You can participate by bidding in the orderbook.</span> </div> <div class="bottombutton-frame"><material-button label="Go to Orderbook" on_click="{gotoSaleAssetOrderbook}" reload_opts="true" is_fullwidth="true"></material-button></div> </div> </div></div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const IS_NOTPC = this.IS_NOTPC = (document.body.clientWidth < WIDTH_PC);
    const commafy = this.commafy = Util.commafy;
    const init = () => {
        this.gotoOrderbook = opts.goto_orderbook;
        this.noimgImg = TagG.RELATIVE_URL_BASE + 'img/noimg.png';
        this.sales = [];
        this.salesToShow = [];
        this.searchedQuery = '';
        this.searchChips = [
            { label: 'Not in Wallet', onClick: () => search('[NINWAL]') },
            { label: 'Auction', onClick: () => search('[AUCTION]') },
            { label: 'NFT', onClick: () => search('[NFT]') },
            { label: 'Locked', onClick: () => search('[LOCKED]') },
            { label: 'TokenVault', onClick: () => search('[TVLT]') },
            { label: 'Rakugaki', onClick: () => search('Rakugaki [NFT]') },
            { label: 'OR', onClick: () => search('オダイロイド１号 OR モナパちゃん OR Tiproid') },
        ];
        if (TagG.query[TagG.QUERYKEY_SEARCH]) {
            this.searchedQuery = decodeURIComponent(TagG.query[TagG.QUERYKEY_SEARCH].replace(/\+/g, ' '));
            delete TagG.query[TagG.QUERYKEY_SEARCH];
        }
    };
    this.treasuresURL = (asset) => TagG.TREASURES_URL_FORMAT.replace('{ASSET}', asset);
    this.onInputSearch = (query) => {
        query = query.trim();
        if (query === this.searchedQuery) return;
        search(query);
    };
    this.onClickClearSearchButton = () => {
        if (this.searchedQuery === '') return;
        search('');
    };
    this.onInputBuyAmount = () => {
        setBuyTotalAmount();
        this.update();
    };
    this.onClickBuyButton = async() => {
        const amount = Number(this.refs.buy_amount.getValue());
        if (isNaN(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量に変な値が入っています' : 'Invalid amount.', 'error');
        if (amount <= 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は正の値で入力してください' : 'Invalid amount.', 'error');
        const totalAmount = TagG.qty2amount(calcTotalSatToBuy(amount), TagG.isQuoteAssetDivisible);
        if (!this.saleAssetInfo.isDivisible && !Number.isInteger(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は整数で入力してください' : 'Invalid amount.', 'error');
        if (!TagG.isQuoteAssetDivisible && !Number.isInteger(totalAmount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'Totalが整数になるように入力してください' : '"Total" must be an integer.', 'error');
        const giveSat = TagG.amount2qty(totalAmount, TagG.isQuoteAssetDivisible);
        const getSat = TagG.amount2qty(amount, this.saleAssetInfo.isDivisible);
        const success = await TagG.aMonapartyOrder(TagG.quoteAsset, giveSat, this.saleAssetInfo.asset, getSat);
        if (!success) return;
        swal.close();
    };
    this.on('mount', async() => {
        TagG.onLoadWalletManually = () => { aShowSales() };
        if (!TagG.wallet) await TagG.aLoadWallet();
        else if (TagG.address) await TagG.aUpdateBalances();
        await aShowSales();
    });
    const aShowSales = async() => {
        try {
            this.refs.spinner.start();
            const params = {
                'order_by': 'block_index',
                'order_dir': 'desc',
                'status': 'open',
                'filters': [
                    { field: 'get_asset', op: '==', value: TagG.quoteAsset },
                    { field: 'give_asset', op: '!=', value: TagG.BASECHAIN_ASSET },
                ],
            };
            const orders = await Monaparty.aPartyTableAll('get_orders', params);
            const assets = orders.map(order => order.give_asset);
            await TagG.aLoadAssets(assets);
            const asset2lowestOrder = {};
            for (const order of orders) {
                order.rawPrice = order.get_quantity / order.give_quantity;
                const lowestOrder = asset2lowestOrder[order.give_asset];
                if (!lowestOrder || order.rawPrice < lowestOrder.rawPrice) {
                    order.isLowest = true;
                    asset2lowestOrder[order.give_asset] = order;
                    if (lowestOrder) lowestOrder.isLowest = false;
                }
            }
            this.sales = orders.filter(order => order.isLowest).map(order => {
                const assetInfo = TagG.asset2info[order.give_asset];
                const amount = TagG.qty2amount(order.give_remaining, assetInfo.isDivisible);
                const getQtyN = BigInt(order.get_quantity);
                const giveQtyN = BigInt(order.give_quantity);
                let priceQtyN;
                if (assetInfo.isDivisible) {
                    priceQtyN = getQtyN * BigInt(TagG.SATOSHI_RATIO) / giveQtyN;
                    if (getQtyN * BigInt(TagG.SATOSHI_RATIO) % giveQtyN) priceQtyN++;
                }
                else {
                    priceQtyN = getQtyN / giveQtyN;
                    if (getQtyN % giveQtyN) priceQtyN++;
                }
                const priceQty = Number(priceQtyN);
                const price = TagG.qty2amount(priceQty, TagG.isQuoteAssetDivisible);
                const priceS = commafy(price);
                const image = TagG.getAssetImageURL('auto', assetInfo.asset);
                const imageL = TagG.getAssetImageURL('L', assetInfo.asset);
                let treasureAmountsS = null;
                if (TagG.asset2treasureQtys[assetInfo.asset]) treasureAmountsS = TagG.asset2treasureQtys[assetInfo.asset].
                    map(qty => commafy(TagG.qty2amount(qty, assetInfo.isDivisible))).join(', ');
                const isInWallet = Boolean(TagG.asset2qty[assetInfo.asset]);
                const isAuction = (price === TagG.AUCTIONFLAG_PRICE);
                const searchText = getSearchText(assetInfo, isAuction);
                const sale = { assetInfo, price, priceS, amount, searchText, image, imageL, treasureAmountsS, isInWallet, isAuction };
                sale.onClick = () => { openBuyWindow(sale) };
                return sale;
            });
            search(this.searchedQuery);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。ヒント: ${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { this.refs.spinner.stop() }
    };
    const openBuyWindow = (sale) => {
        this.saleData = sale;
        this.saleAssetInfo = sale.assetInfo;
        this.saleCard = TagG.asset2card[sale.assetInfo.name];
        this.quoteAssetBalance = TagG.qty2amount(TagG.asset2qty[TagG.quoteAsset] || 0, TagG.isQuoteAssetDivisible);
        this.saleAssetBalance = TagG.qty2amount(TagG.asset2qty[this.saleAssetInfo.asset] || 0, this.saleAssetInfo.isDivisible);
        this.gotoSaleAssetOrderbook = () => {
            swal.close();
            this.gotoOrderbook(this.saleAssetInfo.name);
        };
        this.update();
        if (TagG.address && !sale.isAuction) {
            const defaultAmount = Math.min(sale.amount, 1);
            this.refs.buy_amount.setValue(defaultAmount);
            setBuyTotalAmount();
            this.update();
        }
        Util.aOpenContentSwal(this.refs.buy_window);
    };
    const setBuyTotalAmount = () => {
        const amount = Number(this.refs.buy_amount.getValue());
        if (isNaN(amount)) this.buyTotalAmount = 0;
        else this.buyTotalAmount = TagG.qty2amount(calcTotalSatToBuy(amount), TagG.isQuoteAssetDivisible);
    };
    const calcTotalSatToBuy = (amount) => {
        let totalSat = TagG.amount2qty(this.saleData.price * amount, TagG.isQuoteAssetDivisible);
        if (this.saleAssetInfo.isDivisible) {
            const priceSatN = BigInt(TagG.amount2qty(this.saleData.price, TagG.isQuoteAssetDivisible));
            const amountSatN = BigInt(amount * TagG.SATOSHI_RATIO);
            if (priceSatN * amountSatN > BigInt(totalSat) * BigInt(TagG.SATOSHI_RATIO)) totalSat++;
        }
        return totalSat;
    };
    const getSearchText = (assetInfo, isAuction) => {
        const cardData = TagG.asset2card[assetInfo.name];
        const lockFlag = assetInfo.isLocked ? '[LOCKED]' : '[NLOCKED]';
        const tokenVaultFlag = TagG.asset2treasureQtys[assetInfo.asset] ? '[TVLT]' : '[NTVLT]';
        const inwalletFlag = TagG.asset2qty[assetInfo.asset] ? '[INWAL]' : '[NINWAL]';
        const auctionFlag = isAuction ? '[AUCTION]' : '';
        let text = `${assetInfo.name.toLowerCase()} ${assetInfo.name} ${assetInfo.asset} ${assetInfo.owner} ${assetInfo.description} ${lockFlag} ${tokenVaultFlag} ${inwalletFlag} ${auctionFlag}`;
        if (assetInfo.group) text += ` ${assetInfo.group} [NFT]`;
        else text += ` [FT]`;
        if (!cardData) return text;
        text += ` ${cardData.cardName} ${cardData.ownerName} ${cardData.description} ${cardData.tags}`;
        return text;
    };
    const search = (query) => {
        this.searchedQuery = query;
        let isOR = false;
        let words = query.split(/\s+OR\s+/).map(word => word.trim()).filter(word => word);
        if (words.length > 1) isOR = true;
        else words = query.split(/\s+/).filter(word => word);
        let filteredList;
        if (words.length === 0) filteredList = this.sales;
        else {
            if (isOR) {
                filteredList = this.sales.filter(dispenser => {
                    for (const word of words) {
                        if (dispenser.searchText.includes(word)) return true;
                    }
                    return false;
                });
            }
            else {
                filteredList = this.sales.filter(dispenser => {
                    for (const word of words) {
                        if (!dispenser.searchText.includes(word)) return false;
                    }
                    return true;
                });
            }
        }
        if (this.refs.search_input && this.refs.search_input.getValue() !== query) this.refs.search_input.setValue(query);
        setURLQuery(query);
        const FIRST = 30;
        const ONCE = 300;
        this.salesToShow = filteredList.slice(0, FIRST);
        this.update();
        let length = FIRST;
        const showMore = () => {
            if (length >= filteredList.length || query !== this.searchedQuery) return;
            if (!TagG.IS_LAZYLOADAVAILABLE && length > 300) return;
            length += ONCE;
            this.salesToShow = filteredList.slice(0, length);
            this.update();
            setTimeout(showMore, 0);
        };
        setTimeout(showMore, 0);
    };
    const setURLQuery = (query) => {
        const urlObj = new URL(location);
        if (query) urlObj.searchParams.set(TagG.QUERYKEY_SEARCH, query);
        else urlObj.searchParams.delete(TagG.QUERYKEY_SEARCH);
        history.replaceState(null, '', urlObj.toString());
    };
    init();
});
riot.tag2('base-body', '<div ref="body" class="body-frame"> <div class="body-content"> <base-pagecontroller on_changepage="{onChangePage}"></base-pagecontroller> </div> <div class="body-header"> <h1 class="body-title"><img if="{!subscript}" riot-src="{TagG.RELATIVE_URL_BASE + \'img/gori_80.png\'}" class="body-title-logo clickable" loading="lazy" onclick="{onClickLogo}"><span class="clickable" onclick="{onClickLogo}">GoriFi</span></h1> <div if="{!TagG.address}" class="body-address"><material-button label="Connect Wallet" on_click="{onClickConnectWalletButton}" is_unelevated="true"></material-button></div> <div if="{TagG.address}" class="body-address"><material-button label="{TagG.shortenAddr}" icon="account_balance_wallet" on_click="{onClickShowWalletButton}"></material-button></div> </div> <util-spinner ref="spinner" is_fulldisplay="true"></util-spinner> </div> <div ref="swalwindows_container" show="{false}"> <div ref="about_window"> <div class="basic"> <span if="{TagG.jp}">MonapartyのDEX体験をハッピーにするゴリファイです</span> <span if="{!TagG.jp}">This is GoriFi to make your Monaparty DEX experience happy!</span> </div> <div class="section"> <div class="basic">Don\'t Trust. Verify.</div> <div class="basic"><material-button label="Code Repository" icon="code" is_link="true" link_href="https://bitbucket.org/anipopina/gorifi/" link_target="_blank"></material-button></div> </div> <div class="section"> <div class="basic"> <span if="{TagG.jp}">GoriFiのロゴの元となった素敵ゴリラ</span> <span if="{!TagG.jp}">The lovely gorilla from which the GoriFi logo was derived:</span> </div> <img class="gorilla-img" riot-src="{TagG.RELATIVE_URL_BASE + \'img/gorilla.png\'}" loading="lazy"> <div class="basic"><material-button label="ゴリラ@ぴよたそ" icon="local_florist" is_link="true" link_href="https://hiyokoyarou.com/gorilla/" link_target="_blank"></material-button></div> </div> <div class="section"> <div class="basic">Monaparty Servers:</div> <div class="basic" each="{monapaServers}"><a href="{url}" target="_blank">{host}</a></div> </div> <div class="section"> <div class="basic"> <span if="{TagG.jp}">MONAでもXMPでもその他でもこちらのアドレスに寄付してくれると開発運営が捗ります</span> <span if="{!TagG.jp}">Donations of MONA, XMP, or any tokens can be made to the address below:</span> </div> <div class="qrcode"><util-qrcode qr_value="{TagG.DONATION_ADDR}"></util-qrcode></div> <div class="basic">{TagG.DONATION_ADDR}</div> </div> </div> <div ref="addr_window"> <div class="basic">{TagG.address}</div> <div if="{TagG.address}" class="qrcode"><util-qrcode qr_value="{TagG.address}"></util-qrcode></div> <div class="basic"><i class="material-icons inline-icon">account_balance_wallet</i>&ensp;{commafy(quoteAssetBalance)} {TagG.quoteAsset}</div> <div class="basic"><material-button label="MPCHAIN" icon="search" is_link="true" link_href="{TagG.addrExpURL}" link_target="_blank" reload_opts="true"></material-button></div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.commafy = Util.commafy;
    this.onClickLogo = () => { Util.aOpenContentSwal(this.refs.about_window) };
    this.onClickConnectWalletButton = async() => {
        await TagG.aLoadWallet(true);
        if (!TagG.wallet) TagG.jp?
            myswal('ウォレットに接続できません', '有効なウォレットが検出できませんでした。モナパレットのAppConnectからこのサイトを開くか、ブラウザにMpurseをインストールしてください', 'warning'):
            myswal('Cannot connect wallet', 'A supported wallet could not be detected. Please open this page from Monapalette\'s AppConnect or install Mpurse in your browser.', 'warning');
    };
    this.onClickShowWalletButton = () => {
        this.quoteAssetBalance = TagG.qty2amount(TagG.asset2qty[TagG.quoteAsset] || 0, TagG.isQuoteAssetDivisible);
        this.update();
        Util.aOpenContentSwal(this.refs.addr_window);
    };
    this.on('mount', async() => {
        TagG.body = this;
        TagG.bodySpinner = this.refs.spinner;
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
        const urls = await Monaparty.aGetEndpoints();
        this.monapaServers = urls.map(url => ({ url, host: (new URL(url).host) }));
        this.update();
    });
});
riot.tag2('base-pagecontroller', '<div ref="frame" class="pagecontroller-frame"> <markets-page if="{page === \'markets\'}" tab="{pageParams[0]}" asset1="{pageParams[1]}"></markets-page> </div>', '', '', function(opts) {
    route('/', () => { this.changePage('markets', 'SALES') });
    route('/sales', () => { if (this.page !== 'markets') this.changePage('markets', 'SALES') });
    route('/offers', () => { if (this.page !== 'markets') this.changePage('markets', 'OFFERS') });
    route('/orderbook', () => { if (this.page !== 'markets') this.changePage('markets', 'ORDERBOOK') });
    route('/orderbook/*', (param1) => {
        if (this.page !== 'markets') this.changePage('markets', 'ORDERBOOK', decodeURIComponent(param1));
    });
    route('/..', () => { route('/') });
    route.start(true);

    const _onChangePage = opts.on_changepage;
    this.page = opts.page || '';

    this.changePage = (page, ...pageParams) => {
        if (swal.getState().isOpen) swal.close();
        const frame = this.refs.frame;
        if (_onChangePage) _onChangePage();
        frame.style.pointerEvents = 'none';
        frame.style.opacity = '0.0';
        window.scrollTo(0, 0);
        this.page = page;
        this.pageParams = pageParams;
        this.update();
        TweenLite.to(frame, 0.5, {
            opacity: '1.0',
            onComplete: () => { frame.style.pointerEvents = 'auto'; },
        });
    };
});
riot.tag2('material-button', '<button if="{!isLink}" ref="button" onclick="{onClick}" class="mdc-button {mdcOptClass} {mdccustom-button-white: isWhite, mdccustom-button-fullwidth: isFullWidth}"> <div class="mdc-button__ripple"></div> <i if="{icon}" class="material-icons mdc-button__icon" aria-hidden="true">{icon}</i> <span class="mdc-button__label">{label}</span> </button> <a if="{isLink}" ref="button" href="{linkHref}" target="{linkTarget}" class="mdc-button {mdcOptClass} {mdccustom-button-white: isWhite, mdccustom-button-fullwidth: isFullWidth}"> <div class="mdc-button__ripple"></div> <i if="{icon}" class="material-icons mdc-button__icon" aria-hidden="true">{icon}</i> <span class="mdc-button__label">{label}</span> </a>', '', '', function(opts) {
    const loadOpts = () => {
        this.label = opts.label || '';
        this.icon = opts.icon || null;
        if (opts.is_outlined) this.mdcOptClass = 'mdc-button--outlined';
        if (opts.is_raised) this.mdcOptClass = 'mdc-button--raised';
        if (opts.is_unelevated) this.mdcOptClass = 'mdc-button--unelevated';
        this.isWhite = Boolean(opts.is_white);
        this.isFullWidth = Boolean(opts.is_fullwidth);
        this.isLink = Boolean(opts.is_link);
        this.onClick = opts.on_click;
        this.linkHref = opts.link_href;
        this.linkTarget = opts.link_target;
        this.isDisabled = Boolean(opts.is_disabled);
    };
    this.setDisabled = (isDisabled) => {
        this.refs.button.disabled = isDisabled;
        this.isDisabled = isDisabled;
    };
    this.setLabel = (label, icon = 'NO_CHANGE') => {
        this.label = label;
        if (icon !== 'NO_CHANGE') this.icon = icon;
        this.update();
    };
    this.on('mount', () => {
        this.refs.button.disabled = this.isDisabled;
        new mdc.ripple.MDCRipple(this.refs.button);
    });
    this.on('update', () => {
        if (opts.reload_opts) {
            loadOpts();
            this.refs.button.disabled = this.isDisabled;
        }
    });
    loadOpts();
});
riot.tag2('material-checkbox', '<div ref="formfield" class="mdc-form-field"> <div ref="checkbox" class="mdc-checkbox"> <input type="checkbox" class="mdc-checkbox__native-control" id="{random + \'_checkbox\'}" onchange="{onChange}"> <div class="mdc-checkbox__background"> <svg class="mdc-checkbox__checkmark" viewbox="0 0 24 24"> <path class="mdc-checkbox__checkmark-path" fill="none" d="M1.73,12.91 8.1,19.28 22.79,4.59"></path> </svg> <div class="mdc-checkbox__mixedmark"></div> </div> <div class="mdc-checkbox__ripple"></div> </div> <label if="{label}" for="{random + \'_checkbox\'}" class="mdccustom-label">{label}</label> </div>', '', '', function(opts) {
    const main = () => {
        this.label = opts.label || '';
        const isDefaultChecked = Boolean(opts.is_checked);
        const _onChange = opts.on_change || (checked => { checked });
        this.random = generateID(16);
        let mdcCheckbox = null;

        this.getChecked = () => {
            if (mdcCheckbox) return mdcCheckbox.checked;
            else return null;
        };
        this.setChecked = (checked) => {
            if (mdcCheckbox) mdcCheckbox.checked = checked;
            else setTimeout(() => {
                if (mdcCheckbox) mdcCheckbox.checked = checked;
            }, 0);
        };

        this.onChange = () => { _onChange(mdcCheckbox.checked) };
        this.on('mount', () => {
            mdcCheckbox = new mdc.checkbox.MDCCheckbox(this.refs.checkbox);
            const mdcFormField = new mdc.formField.MDCFormField(this.refs.formfield);
            mdcFormField.input = mdcCheckbox;
            mdcCheckbox.checked = isDefaultChecked;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    main();
});
riot.tag2('material-chip', '<div ref="chip" class="mdc-chip" onclick="{onClick}"> <div class="mdc-chip__ripple"></div> <i if="{icon}" class="material-icons mdc-chip__icon mdc-chip__icon--leading">{icon}</i> <span class="mdc-chip__primary-action"> <span class="mdc-chip__text">{label}</span> </span> </div>', '', '', function(opts) {
    const loadOpts = () => {
        this.label = opts.label || '';
        this.icon = opts.icon || null;
        this.onClick = opts.on_click;
    }
    this.setLabel = (label, icon = 'NO_CHANGE') => {
        this.label = label;
        if (icon !== 'NO_CHANGE') this.icon = icon;
        this.update();
    };
    this.on('mount', () => {
        new mdc.chips.MDCChip(this.refs.chip);
    });
    this.on('update', () => {
        if (opts.reload_opts) loadOpts();
    });
    loadOpts();
});
riot.tag2('material-iconbutton', '<button if="{!isLink}" ref="button" onclick="{onClick}" class="mdc-icon-button material-icons {mdcOptClass} {mdccustom-iconbutton-white: isWhite}">{icon}</button> <a if="{isLink}" ref="button" href="{linkHref}" target="{linkTarget}" class="mdc-icon-button material-icons {mdcOptClass} {mdccustom-iconbutton-white: isWhite}">{icon}</a>', '', '', function(opts) {
    const loadOpts = () => {
        this.icon = opts.icon || null;
        this.isWhite = Boolean(opts.is_white);
        this.isLink = Boolean(opts.is_link);
        this.onClick = opts.on_click;
        this.linkHref = opts.link_href;
        this.linkTarget = opts.link_target;
        this.isDisabled = Boolean(opts.is_disabled);
    };
    this.setDisabled = (isDisabled) => {
        this.refs.button.disabled = isDisabled;
        this.isDisabled = isDisabled;
    };
    this.setIcon = (icon) => { this.icon = icon; this.update() };
    this.on('mount', () => {
        this.refs.button.disabled = this.isDisabled;
        const ripple = new mdc.ripple.MDCRipple(this.refs.button);
        ripple.unbounded = true;
    });
    this.on('update', () => {
        if (opts.reload_opts) {
            loadOpts();
            this.refs.button.disabled = this.isDisabled;
        }
    });
    loadOpts();
});
riot.tag2('material-radiobuttons', '<div each="{items}" ref="{formFieldRef}" class="mdc-form-field {mdccustom-formfield-block: isBlock}"> <div ref="{radioRef}" class="mdc-radio"> <input ref="{radioInputRef}" class="mdc-radio__native-control" type="radio" id="{id}" riot-value="{value}" name="{name}" onchange="{onChange}"> <div class="mdc-radio__background"> <div class="mdc-radio__outer-circle"></div> <div class="mdc-radio__inner-circle"></div> </div> <div class="mdc-radio__ripple"></div> </div> <label for="{id}" class="mdccustom-label">{label}</label> </div>', '', '', function(opts) {
    const main = () => {
        this.items = deepCopy(opts.items || [{ value: 'value1', label: 'item1' }]);
        const defaultValue = opts.default_value || null;
        this.isBlock = Boolean(opts.is_block);
        const _onChange = opts.on_change || (value => { value });
        for (const item of this.items) {
            const random = generateID(16);
            item.id = random;
            item.formFieldRef = `${random}_formfield`;
            item.radioRef = `${random}_radio`;
            item.radioInputRef = `${random}_radioinput`;
        }
        this.value = this.items[0] ? this.items[0].value : null;
        this.name = generateID(16);

        this.getValue = () => this.value;
        this.setValue = (value) => {
            const item = this.items.find(item => (item.value === value));
            if (!item) return;
            if (item.mdcRadio) {
                item.mdcRadio.checked = true;
                this.value = value;
            }
            else setTimeout(() => {
                if (item.mdcRadio) {
                    item.mdcRadio.checked = true;
                    this.value = value;
                }
            }, 0);
        };

        this.onChange = (event) => {
            const sender = event.target || event.srcElement;
            this.value = sender.value;
            _onChange(this.value);
        };
        this.on('mount', () => {
            for (const item of this.items) {
                item.mdcRadio = new mdc.radio.MDCRadio(this.refs[item.radioRef]);
                const formField = new mdc.formField.MDCFormField(this.refs[item.formFieldRef]);
                formField.input = item.mdcRadio;
            }
            if (defaultValue) this.setValue(defaultValue);
            else if (this.items[0]) this.items[0].mdcRadio.checked = true;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    const deepCopy = (array) => JSON.parse(JSON.stringify(array));
    main();
});
riot.tag2('material-select', '<div ref="select" class="mdc-select mdc-select--filled {mdccustom-select-fullwidth: isFullWidth}"> <div class="mdc-select__anchor" role="button" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="{random + \'_label\'} {random + \'_selectedtext\'}"> <span class="mdc-select__ripple"></span> <span id="{random + \'_label\'}" class="mdc-floating-label">{hint}</span> <span class="mdc-select__selected-text-container"> <span id="{random + \'_selectedtext\'}" class="mdc-select__selected-text"></span> </span> <span class="mdc-select__dropdown-icon"> <svg class="mdc-select__dropdown-icon-graphic" viewbox="7 10 10 5" focusable="false"> <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon> <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon> </svg> </span> <span class="mdc-line-ripple"></span> </div> <div class="mdc-select__menu mdc-menu mdc-menu-surface {mdc-menu-surface--fullwidth: isMenuWidthFixed}"> <ul class="mdc-list" role="listbox" aria-label="Food picker listbox"> <li if="{withEmptyItem}" class="mdc-list-item mdc-list-item--selected" aria-selected="true" data-value="" role="option"> <span class="mdc-list-item__ripple"></span> </li> <li each="{items}" class="mdc-list-item" aria-selected="false" data-value="{value}" role="option"> <span class="mdc-list-item__ripple"></span> <span class="mdc-list-item__text">{label}</span> </li> </ul> </div> </div>', '', '', function(opts) {
    const main = () => {
        this.hint = opts.hint || '';
        this.items = copyItems(opts.items || [{ value: 'value1', label: 'item1' }]);
        const defaultValue = opts.default_value || null;
        const width = opts.width || null;
        this.isFullWidth = Boolean(opts.is_fullwidth);
        this.withEmptyItem = Boolean(opts.with_empty_item);
        this.isMenuWidthFixed = Boolean(opts.is_menuwidth_fixed);
        const _onChange = opts.on_change || (value => { value });
        this.random = generateID(16);
        let mdcSelect = null;

        this.getValue = () => {
            if (mdcSelect) return mdcSelect.value;
            else return null;
        };
        this.setValue = (value) => {
            if (mdcSelect) mdcSelect.value = value;
            else setTimeout(() => {
                if (mdcSelect) mdcSelect.value = value;
            }, 0);
        };

        const onChange = () => { _onChange(mdcSelect.value) };
        this.on('mount', () => {
            this.refs.select.style.width = width;
            mdcSelect = new mdc.select.MDCSelect(this.refs.select);
            if (defaultValue) mdcSelect.value = defaultValue;
            mdcSelect.listen('MDCSelect:change', onChange);
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    const copyItems = (items) => JSON.parse(JSON.stringify(items.map(({ value, label }) => ({ value, label }))));
    main();
});
riot.tag2('material-switch', '<div ref="switch" class="mdc-switch"> <div class="mdc-switch__track"></div> <div class="mdc-switch__thumb-underlay"> <div class="mdc-switch__thumb"></div> <input type="checkbox" id="{random + \'_switch\'}" class="mdc-switch__native-control" role="switch" aria-checked="false" onchange="{onChange}"> </div> </div> <label for="{random + \'_switch\'}" class="mdccustom-label">{label}</label>', '', '', function(opts) {
    const main = () => {
        this.label = opts.label || '';
        const isDefaultChecked = Boolean(opts.is_checked);
        const _onChange = opts.on_change || (checked => { checked });
        this.random = generateID(16);
        let mdcSwitch = null;

        this.getChecked = () => {
            if (mdcSwitch) return mdcSwitch.checked;
            else return null;
        };
        this.setChecked = (checked) => {
            if (mdcSwitch) mdcSwitch.checked = checked;
            else setTimeout(() => {
                if (mdcSwitch) mdcSwitch.checked = checked;
            }, 0);
        };

        this.onChange = () => { _onChange(mdcSwitch.checked) };
        this.on('mount', () => {
            mdcSwitch = new mdc.switchControl.MDCSwitch(this.refs.switch);
            mdcSwitch.checked = isDefaultChecked;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    main();
});
riot.tag2('material-tabbar', '<div ref="tabbar" class="mdc-tab-bar" role="tablist"> <div class="mdc-tab-scroller"> <div class="mdc-tab-scroller__scroll-area"> <div class="mdc-tab-scroller__scroll-content"> <button each="{items}" class="mdc-tab" role="tab" aria-selected="false" tabindex="{index}"> <span class="mdc-tab__content"> <span if="{icon}" class="mdc-tab__icon material-icons" aria-hidden="true">{icon}</span> <span class="mdc-tab__text-label">{label}</span> </span> <span class="mdc-tab-indicator"> <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span> </span> <span class="mdc-tab__ripple"></span> </button> </div> </div> </div> </div>', '', '', function(opts) {
    const main = () => {
        this.items = deepCopy(opts.items || [{ value: 'value1', label: 'item1', icon: 'favorite' }]);
        const defaultValue = opts.default_value || null;
        const onChange = opts.on_change || (value => { value });
        this.items.forEach((item, index) => { item.index = index });
        this.value = this.items[0] ? this.items[0].value : null;
        let mdcTabbar = null;

        this.getValue = () => this.value;
        this.setValue = (value) => {
            const index = value2index(value);
            if (index === -1) return;
            if (mdcTabbar) activateIndex(index);
            else setTimeout(() => {
                if (mdcTabbar) activateIndex(index);
            }, 0);
        };

        const onActivated = ({ detail }) => {
            this.value = this.items[detail.index].value;
            onChange(this.value);
        };
        this.on('mount', () => {
            mdcTabbar = new mdc.tabBar.MDCTabBar(this.refs.tabbar);
            const index = defaultValue ? value2index(defaultValue) : 0;
            activateIndex(index);
            mdcTabbar.listen('MDCTabBar:activated', onActivated);
        });
        const value2index = (value) => this.items.findIndex(item => (item.value === value));
        const activateIndex = (index) => {
            mdcTabbar.activateTab(index);
            mdcTabbar.scrollIntoView(index);
            this.value = this.items[index].value;
        };
    };
    const deepCopy = (array) => JSON.parse(JSON.stringify(array));
    main();
});
riot.tag2('material-textfield', '<label if="{!isOutlined}" ref="textfield" class="mdc-text-field mdc-text-field--filled {mdc-text-field--textarea: isTextarea}"> <span class="mdc-text-field__ripple"></span> <span if="{hint}" class="mdc-floating-label" id="{random + \'_label\'}">{hint}</span> <input if="{!isTextarea}" ref="input" class="mdc-text-field__input" type="{isPassword ? \'password\' : \'text\'}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"> <textarea if="{isTextarea}" ref="input" class="mdc-text-field__input" rows="{textareaRows}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"></textarea> <span class="mdc-line-ripple"></span> </label> <label if="{isOutlined}" ref="textfield" class="mdc-text-field mdc-text-field--outlined {mdc-text-field--textarea: isTextarea}"> <span class="mdc-notched-outline"> <span class="mdc-notched-outline__leading"></span> <span if="{hint}" class="mdc-notched-outline__notch"> <span class="mdc-floating-label" id="{random + \'_label\'}">{hint}</span> </span> <span class="mdc-notched-outline__trailing"></span> </span> <input if="{!isTextarea}" ref="input" class="mdc-text-field__input" type="{isPassword ? \'password\' : \'text\'}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"> <textarea if="{isTextarea}" ref="input" class="mdc-text-field__input" rows="{textareaRows}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"></textarea> </label> <div class="mdc-text-field-helper-line"> <div class="mdc-text-field-helper-text {mdc-text-field-helper-text--persistent: isHelperPersistent || !isValid, mdc-text-field-helper-text--validation-msg: !isValid}" id="{random + \'_helper\'}" aria-hidden="true"></div> <div if="{maxLength}" class="mdc-text-field-character-counter">0 / {maxLength}</div> </div>', '', '', function(opts) {
    const main = () => {
        this.hint = opts.hint || '';
        let helperMessage = opts.helper || '';
        const invalidMessage = opts.invalid_message || 'invalid input';
        const patternReg = opts.pattern ? new RegExp(`^${opts.pattern}\$`) : null;
        const checkFunc = opts.check_func;
        this.maxLength = opts.max_length || null;
        this.isOutlined = Boolean(opts.is_outlined);
        this.isHelperPersistent = Boolean(opts.is_helper_persistent);
        this.isPassword = Boolean(opts.is_password);
        this.isTextarea = Boolean(opts.is_textarea);
        this.textareaRows = opts.textarea_rows || '4';
        const onEnter = opts.on_enter || (value => { value });
        const _onChange = opts.on_change || (value => { value });
        const _onInput = opts.on_input || (value => { value });
        this.random = generateID(16);
        this.isValid = true;
        let mdcTextField = null;

        this.getValue = () => {
            if (mdcTextField) return mdcTextField.value;
            else return null;
        };
        this.setValue = (value) => {
            if (mdcTextField) mdcTextField.value = value;
            else setTimeout(() => {
                if (mdcTextField) mdcTextField.value = value;
            }, 0);
        };
        this.setHelperMessage = (message) => {
            helperMessage = message;
            if (mdcTextField.valid) mdcTextField.helperTextContent = helperMessage;
        };
        this.focus = () => { mdcTextField.focus() };

        this.onChange = () => {
            const inputValue = mdcTextField.value;
            const lastValid = this.isValid;
            if (checkFunc) this.isValid = checkFunc(inputValue);
            else if (patternReg) this.isValid = Boolean(inputValue.match(patternReg));
            if (this.isValid !== lastValid) {
                this.update();
                mdcTextField.helperTextContent = this.isValid ? helperMessage : invalidMessage;
                mdcTextField.valid = this.isValid;
            }
            _onChange(inputValue);
        };
        this.onInput = () => { _onInput(mdcTextField.value) };
        this.onKeyPress = (event) => {
            if (event.code === 'Enter' || event.key === 'Enter' || event.keyCode === 13) {
                onEnter(mdcTextField.value);
            }
        };
        this.on('mount', () => {
            mdcTextField = new mdc.textField.MDCTextField(this.refs.textfield);
            mdcTextField.useNativeValidation = false;
            mdcTextField.helperTextContent = helperMessage;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    main();
});
riot.tag2('util-qrcode', '<svg xmlns="http://www.w3.org/2000/svg" class="svg-frame"> <g ref="qrcode"></g> </svg>', '', '', function(opts) {
    this.value = opts.qr_value;

    this.getValue = () => {
        return this.value;
    };
    this.setValue = value => {
        this.value = value;
        this.updateQR();
    };
    this.updateQR = () => {
        if (this.value) this.qrcode.makeCode(this.value);
    };
    this.on('mount', () => {
        this.qrcode = new QRCode(this.refs.qrcode, {
            width: 400,
            height: 400,
            useSVG: true,
        });
        this.updateQR();
    });
});
riot.tag2('util-spinner', '<div if="{isSpinning}" ref="frame" class="spinner-frame {spinner-frame-fulldisplay: isFullDisplay}"></div>', '', '', function(opts) {
	this.isFullDisplay = Boolean(opts.is_fulldisplay);
	const SPINNER_PARAMS = {
		lines: 9,
		length: 12,
		width: 4,
		radius: 16,
		scale: 1,
		corners: 0.5,
		color: '#AAAAAA',
		opacity: 0.1,
		rotate: 0,
		direction: 1,
		speed: 1.2,
		trail: 60,
		fps: 20,
		zIndex: 2e9,
		className: 'spinner',
		top: '50%',
		left: '50%',
		shadow: false,
		hwaccel: false,
		position: 'absolute'
	};
	this.isSpinning = false;
    this.spinner = new Spinner(SPINNER_PARAMS);
    this.start = () => {
        if (this.isSpinning) return;
        this.isSpinning = true;
        this.update();
        if (this.refs.frame) this.spinner.spin(this.refs.frame);
    };
    this.stop = () => {
        if (!this.isSpinning) return;
        if (this.refs.frame) this.spinner.stop();
        this.isSpinning = false;
        this.update();
    };
});
