// version: 20220323
const DEFAULT_PARAMS = {
    // serversFileがnullの場合はfullEndpoints,nocoindEndpointsが直接使われる
    serversFile: 'https://monapachan.s3.ap-northeast-1.amazonaws.com/monaparty-servers.json',
    fullEndpoints: [
//        'https://counterblock.api.monaparty.me',
        'https://wallet.monaparty.me/_api',
        'https://monapa.electrum-mona.org/_api',
    ],
    nocoindEndpoints : [
        'https://mpchain.info/api/cb',
    ],
    minCounterBlockVer: '1.4.1+mona001',
    minCounterPartyVer: '9.5811.3',
    timeout: 10000,
    maxServerNum: 3,
    COIND_COMMANDS: ['broadcast_tx'],
    UNAVOIDABLE_ERRCODES: [
        -32001, // create_xxx の構成エラー全般
    ],
};

// ブラウザで使うときはbrowserifyにiオプションを付ければnode-fetchは不要  -i node-fetch
const fetch = (typeof window === 'undefined') ? require('node-fetch') : window.fetch;

class Monaparty {
    static async aCounterPartyRequest(command, params, options = {}) {
        const counterBlockParams = { method: command, params };
        return await this.aCounterBlockRequest('proxy_to_counterpartyd', counterBlockParams, options);
    }
    static async aCounterBlockRequest(command, params, { useCache = false, intAsStr = false, tryAll = false, endpoints = null } = {}) {
        const body = { jsonrpc: '2.0', id: 0, method: command, params };
        const bodyStr = JSON.stringify(body);
        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
            body: bodyStr,
        };
        if (useCache && Monaparty.cacheDict[bodyStr]) return Monaparty.cacheDict[bodyStr];
        if (!endpoints) endpoints = await aGetEndpoints(command);
        if (endpoints.length === 0) throw new Error(`Counterblock request failed: No available endpoint`);
        const errors = [];
        for (const api of endpoints) {
            let counterErrMessage, counterErrCode;
            try {
                console.log(`Counterblock request to ${api}: ${bodyStr}`);
                let responseText = await aFetchWithTimeout(Monaparty.timeout, api, fetchParams).then(res => res.text());
                if (intAsStr) responseText = responseText.replace(/([^\\]"\s*:\s*)(\d+)(\s*[,}])/g, '$1"$2"$3');
                const response = JSON.parse(responseText);
                const result = response.result;
                if (result === undefined) {
                    [counterErrMessage, counterErrCode] = parseCounterError(response);
                    if (!counterErrMessage) counterErrMessage = responseText;
                    throw new Error(JSON.stringify({ api, code: counterErrCode , message: counterErrMessage }));
                }
                if (useCache || Monaparty.cacheDict[bodyStr]) Monaparty.cacheDict[bodyStr] = result;
                return result;
            }
            catch (error) {
                errors.push(`${error}`);
                if (!tryAll && Monaparty.UNAVOIDABLE_ERRCODES.includes(counterErrCode)) break;
            }
        }
        throw new Error(`Counterblock request failed: ${JSON.stringify(errors)}`);
    }
    static async aCounterPartyRequestTableAll(command, params, options = {}, { maxPage = 5 } = {}) {
        let allItems = [];
        const prms = {...params};
        prms.limit = 1000;
        for (let i = 0; i < maxPage; i++) {
            prms.offset = prms.limit * i;
            const items = await this.aCounterPartyRequest(command, prms, options);
            allItems = [...allItems, ...items];
            if (items.length < prms.limit) break;
        }
        return allItems;
    }
    static async aGetEndpoints(command = null) {
        return await aGetEndpoints(command);
    }
}
Monaparty.cacheDict = {};
for (const key in DEFAULT_PARAMS) Monaparty[key] = DEFAULT_PARAMS[key];
module.exports = Monaparty;

const aGetEndpoints = async(command) => {
    let endpoints;
    const isCoindCommand = Monaparty.COIND_COMMANDS.includes(command);
    if (isCoindCommand && aGetEndpoints.coindEndpoints) endpoints = aGetEndpoints.coindEndpoints;
    else if (!isCoindCommand && aGetEndpoints.allEndpoints) endpoints = aGetEndpoints.allEndpoints;
    if (!endpoints) {
        const whiteList = isCoindCommand ? Monaparty.fullEndpoints : Monaparty.fullEndpoints.concat(Monaparty.nocoindEndpoints);
        if (!Monaparty.serversFile) return whiteList;
        if (!aGetEndpoints.cachedPromise) aGetEndpoints.cachedPromise = fetch(Monaparty.serversFile).then(res => res.json());
        const { servers } = await aGetEndpoints.cachedPromise;
        const height = servers
            .reduce((maxHeight, server) => (server.counterblockLastBlock > maxHeight ? server.counterblockLastBlock : maxHeight), 0);
        endpoints = servers
            .filter(server => server.successCount)
            .filter(server => (server.counterblockLastBlock >= height - 1))
            .filter(server => (checkCounterBlockVersion(server.counterblockVersion) && checkCounterPartyVersion(server.counterpartyVersion)))
            .map(server => server.endpoint)
            .filter(endpoint => whiteList.includes(endpoint));
        isCoindCommand ? aGetEndpoints.coindEndpoints = endpoints : aGetEndpoints.allEndpoints = endpoints;
    }
    shuffle(endpoints);
    return endpoints.slice(0, Monaparty.maxServerNum);
};
const parseCounterError = (response) => {
    let errMessage = '';
    let code = null;
    if (response.error && response.error.data) {
        errMessage = response.error.data.message;
        try {
            const parsed = JSON.parse(errMessage);
            errMessage = parsed.message;
            code = parsed.code;
        }
        catch (e) {}
    }
    else if (typeof response.data === 'string') {
        errMessage = response.data;
        if (response.code) code = response.code;
    }
    return [errMessage, code];
};
const shuffle = (array) => {
    let n = array.length;
    while (n) {
        const i = Math.floor(Math.random() * n--);
        const value = array[n];
        array[n] = array[i];
        array[i] = value;
    }
};
const aTimeout = (ms, promise) => {
    return new Promise((resolve, reject) => {
        const timeoutID = setTimeout(() => reject(new Error('timeout by aTimeout()')), ms);
        promise.then(resolve).catch(reject).finally(() => { clearTimeout(timeoutID) });
    });
};
const aFetchWithTimeout = async(timeoutMs, url, fetchParams) => {
    return await aTimeout(timeoutMs, fetch(url, fetchParams));
};
const checkCounterBlockVersion = (version) => {
    if (!checkCounterBlockVersion.ZEROPAD_MINVER) checkCounterBlockVersion.ZEROPAD_MINVER = zeropad(Monaparty.minCounterBlockVer);
    return zeropad(version) >= checkCounterBlockVersion.ZEROPAD_MINVER;
};
const checkCounterPartyVersion = (version) => {
    if (!checkCounterPartyVersion.ZEROPAD_MINVER) checkCounterPartyVersion.ZEROPAD_MINVER = zeropad(Monaparty.minCounterPartyVer);
    return zeropad(version) >= checkCounterPartyVersion.ZEROPAD_MINVER;
};
const zeropad = (str) => str.replace(/\d+/g, (match) => `0000000${match}`.slice(-8));
