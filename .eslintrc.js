module.exports = {
    "env": {
        "browser": true,
        "node": true,
        "commonjs": true,
        "es6": true,
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2020
    },
    "rules": {
        "no-console": "off",
        "no-unused-vars": "warn",
        "no-warning-comments": "warn",
        "no-empty": "off",
        "no-fallthrough": "warn",
    }
};
